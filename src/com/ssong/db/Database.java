package com.ssong.db;

/*
 * Database Class의 Method 작성 가이드 라인 (Written by 찬솔)
 * 
 * 1. 메소드의 첫 줄에서 this.Clear()을 할 것.
 * 2. ResultSet rs를 만들어서 사용 할 것.
 * 3. 메소드의 마지막 줄에서 rs.close()을 할 것. (rs를 만들었을 경우에만)
 * 
 * 참고. 예제로 추가된 getSha1 메소드를 참고하여 작성 할 것.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.ssong.bean.DateYearRanking;
import com.ssong.bean.DetailMusic;
import com.ssong.bean.GenreStatistic;
import com.ssong.bean.SimpleMusic;
import com.ssong.bean.SingerStatistic;
import com.ssong.bean.UserFavoriteData;
import com.ssong.bean.UserLogin;
import com.ssong.bean.UserRegister;
import com.ssong.bean.Playlist;
import com.ssong.bean.ListMusic;
import com.ssong.bean.Poem;
import com.ssong.file.FavoriteSpliter;

public class Database {

	static DataSource ds = null;

	private Connection conn = null;

	private Statement stmt = null;
	private PreparedStatement ps = null;

	private String sql = "";

	static {
		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:/comp/env");
			ds = (DataSource) envCtx.lookup("jdbc/mysql_flashback");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Database() {
		try {
			conn = ds.getConnection();
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
		}
	}

	private void Clear() {
		if (stmt != null) {
			try {
				stmt.close();
				stmt = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (ps != null) {
			try {
				ps.close();
				ps = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void Close() {
		this.Clear();
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	// ///////////////////////////////////////////////////////////////////////////////
	// 인자로 전달한 값으로 Sha1을 데이터베이스로 부터 얻어오는 예제용 메소드
	public String getSha1(String value) {

		// Statement 초기화
		this.Clear();

		// ResultSet 선언
		ResultSet rs = null;

		// 기타 변수들 선언
		String Sha1Result = null;

		// 파라미터 값 검증
		if (value == null) {
			return null;
		}

		try {

			sql = "SELECT sha1(?) AS ret;";

			ps = conn.prepareStatement(sql);

			ps.setString(1, value);
			rs = ps.executeQuery();

			while (rs.next()) {
				Sha1Result = rs.getString("ret");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return Sha1Result;
	}

	/**
	 * 회원 가입 로직 sha1
	 * 
	 * @param UserData
	 * @return
	 */
	public boolean registerUser(UserRegister UserData) {
		this.Clear();
		boolean success = false;
		if (UserData.getId() == null || UserData.getPasswd() == null) { // 객체 널
																		// 체크
			return success;
		}
		if (UserData.getId().equals("") || UserData.getPasswd().equals("")) { // 빈문자
																				// 체크
			return success;
		}
		String id = UserData.getId();
		String pw = UserData.getPasswd();
		if (id.length() > 16 // 아이디 길이 체크
				|| id.length() < 4 || pw.length() > 16 || pw.length() < 4) {
			return success;
		}

		boolean user_exist = getUserExist(UserData.getId()); // 아이디 중복 체크

		try {
			if (user_exist == false) { // 항상 0
				System.out.println("회원가입성공");
				sql = "INSERT INTO user(uid, upasswd, unick, uage, ugender) values(?, sha1(?), ?, ?, ?);";
				ps = conn.prepareStatement(sql);
				ps.setString(1, UserData.getId());
				ps.setString(2, UserData.getPasswd());
				ps.setString(3, UserData.getNickname());
				ps.setInt(4, UserData.getAge());
				ps.setInt(5, UserData.getGender());
				ps.executeUpdate();
				success = true;
			} else {
				System.out.println("아이디 중복");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return success;
	}

	/**
	 * 사용자 중복 체크
	 * 
	 * @param id
	 * @return
	 */
	public boolean getUserExist(String id) {
		this.Clear();
		boolean success = false;
		ResultSet rs = null;
		try {
			sql = "SELECT uidx FROM user WHERE uid=?;";
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, id);
			rs = ps.executeQuery();

			if (rs.next()) {
				success = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return success;
	}

	/**
	 * 로그인 처리 함수
	 * 
	 * @param username
	 *            4 ~ 16
	 * @param password
	 *            sha1
	 * @return
	 */
	public UserLogin getUser(String username, String password) {
		this.Clear();
		UserLogin user = null;
		if (username == null || username.equals("")) {
			return user;
		}
		if (password == null || password.equals("")) {
			return user;
		}
		ResultSet rs = null;
		sql = "SELECT uidx, uid, unick, uage, ugender, usurvey "
				+ "FROM user WHERE uid=? AND upasswd=?;";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			rs = ps.executeQuery();
			while (rs.next()) {
				user = new UserLogin(rs.getInt(1), rs.getString(2),
						rs.getString(3), rs.getInt(5), rs.getInt(6),
						rs.getInt(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			user = null;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return user;
	}

	public UserLogin getUser(String username) {
		this.Clear();
		UserLogin user = null;
		if (username == null || username.equals("")) {
			return user;
		}

		ResultSet rs = null;
		sql = "SELECT uidx, uid, unick, uage, ugender, usurvey "
				+ "FROM user WHERE uid=?;";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);

			rs = ps.executeQuery();
			while (rs.next()) {
				user = new UserLogin(rs.getInt(1), rs.getString(2),
						rs.getString(3), rs.getInt(5), rs.getInt(6),
						rs.getInt(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return user;
	}

	/**
	 * 랜덤 음악 120곡을 가져오는 함수
	 * 
	 * @return - 랜덤 음악 120
	 */
	public List<SimpleMusic> getRandomMusic(int uindex) {
		this.Clear();
		ArrayList<SimpleMusic> musicList = null;
		ResultSet rs = null;
		try {
			SimpleMusic music = null;

			sql = new String(
					"select m.midx, m.martist, m.mtitle, a.aimg_path "
							+ " from album as a join music as m on m.maidx = a.aidx "
							+ "where m.midx not in (select rmidx from rating where ruidx = ? ) "
							+ "order by rand() LIMIT 28");
			ps = conn.prepareStatement(sql);
			ps.setInt(1, uindex);
			rs = ps.executeQuery();

			musicList = new ArrayList<SimpleMusic>();
			while (rs.next()) {
				music = new SimpleMusic(rs.getInt(1), rs.getString(2),
						rs.getString(3), rs.getString(4), 0);
				musicList.add(music);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return musicList;
	}

	public boolean hasRating(int uindex, int midx) {
		this.Clear();
		boolean result = false;
		ResultSet rs = null;
		try {
			sql = "select count(1) from rating where ruidx = ? and rmidx = ?";
			ps = conn.prepareStatement(sql);
			// System.out.println("uidx : "+ uindex + " midx : "+ midx);
			ps.setInt(1, uindex);
			ps.setInt(2, midx);
			rs = ps.executeQuery();
			rs.next();
			if (rs.getInt(1) != 0) {
				// System.out.println("has rating " + rs.getInt(1));
				result = true;
			} else {
				result = false;
				// System.out.println("no has rating");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.Clear();
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 아티스트의 곡 개수
	 * 
	 * @param query
	 * @return
	 */
	public int getCountArtistMusicSearch(String query) {
		this.Clear();
		ResultSet rs = null;
		int resultCount = 0;
		try {
			sql = "SELECT count(*) martist from music where martist = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, query);
			// System.out.println("검색 : %"+query+"%");
			rs = ps.executeQuery();
			while (rs.next()) {
				resultCount = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultCount;
	}

	/**
	 * 검색된 아티스트의 개수
	 * 
	 * @param query
	 * @return
	 */
	public int getCountArtistearch(String query) {
		this.Clear();
		ResultSet rs = null;
		int resultCount = 0;
		try {
			sql = "SELECT count(DISTINCT(martist)) martist from music where martist like ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + query + "%");
			// System.out.println("검색 : %"+query+"%");
			rs = ps.executeQuery();
			while (rs.next()) {
				resultCount = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultCount;
	}

	/**
	 * 검색된 음악의 개수
	 * 
	 * @param query
	 * @return
	 */
	public int getCountMusicSearch(String query) {
		this.Clear();
		ResultSet rs = null;
		int resultCount = 0;
		try {
			SimpleMusic music = null;
			sql = "SELECT count(*) from music where mtitle like ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + query + "%");
			// System.out.println("검색 : %"+query+"%");
			rs = ps.executeQuery();
			while (rs.next()) {
				resultCount = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultCount;
	}

	public ArrayList<SimpleMusic> searchArtistMusic(String query, int offset,
			int count) {
		this.Clear();
		ResultSet rs = null;
		ArrayList<SimpleMusic> arr = null;
		try {
			SimpleMusic music = null;
			sql = "select m.midx,  m.martist, m.mtitle, a.aimg_path from music as m"
					+ " inner join album as a on m.maidx=a.aidx"
					+ " where m.martist = ? LIMIT ? offset ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, query);
			ps.setInt(2, count);
			ps.setInt(3, offset);
			rs = ps.executeQuery();
			arr = new ArrayList<SimpleMusic>();
			while (rs.next()) {
				music = new SimpleMusic(rs.getInt(1), rs.getString(2),
						rs.getString(3), rs.getString(4), 0);
				arr.add(music);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return arr;
	}

	/**
	 * LIKE문으로 음악 검색 기능 구현
	 * 
	 * @param query
	 * @return
	 */
	public ArrayList<SimpleMusic> searchMusic(String query, int offset,
			int count) {
		this.Clear();
		ResultSet rs = null;
		ArrayList<SimpleMusic> arr = null;
		try {
			SimpleMusic music = null;
			sql = "SELECT m.midx,  m.martist, m.mtitle, a.aimg_path from album as a "
					+ "inner join music as m on m.maidx=a.aidx where m.mtitle like ? LIMIT ? offset ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + query + "%");
			// System.out.println("검색 : %"+query+"%");
			ps.setInt(2, count);
			ps.setInt(3, offset);
			rs = ps.executeQuery();
			arr = new ArrayList<SimpleMusic>();
			while (rs.next()) {
				music = new SimpleMusic(rs.getInt(1), rs.getString(2),
						rs.getString(3), rs.getString(4), 0);
				arr.add(music);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return arr;
	}

	/**
	 * LIKE문으로 검색 기능 구현
	 * 
	 * @param query
	 * @return
	 */
	public ArrayList<SimpleMusic> searchArtist(String query, int offset,
			int count) {
		this.Clear();
		ResultSet rs = null;
		ArrayList<SimpleMusic> arr = null;
		try {
			SimpleMusic music = null;
			sql = "select m.martist, a.aimg_path from music as m"
					+ " inner join album as a on m.maidx=a.aidx"
					+ " where m.martist like ? group by m.martist LIMIT ? offset ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, '%' + query + '%');
			ps.setInt(2, count);
			ps.setInt(3, offset);
			rs = ps.executeQuery();
			arr = new ArrayList<SimpleMusic>();
			while (rs.next()) {
				music = new SimpleMusic(0, rs.getString(1), null,
						rs.getString(2), 0);
				arr.add(music);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return arr;
	}

	/**
	 * 장르 통계 데이터를 가져온다.
	 * 
	 * @param uidx
	 *            - 사용자 인덱스
	 * @return
	 */
	public ArrayList<GenreStatistic> getGenrePercent(int uidx) {
		this.Clear();
		ArrayList<GenreStatistic> arr = null;
		ResultSet rs = null;
		try {
			GenreStatistic statistic = null;
			sql = "SELECT m.mgenre, count(mdate_year) as count FROM rating as r "
					+ "inner join music as m on m.midx=r.rmidx "
					+ "where r.ruidx = ? and r.rmusic_rating >? group by m.mgenre "
					+ "order by count desc limit 8";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, uidx);
			ps.setFloat(2, FavoriteSpliter.MIN_RATING);
			rs = ps.executeQuery();
			arr = new ArrayList<GenreStatistic>();
			while (rs.next()) {
				statistic = new GenreStatistic(rs.getString(1), rs.getInt(2));
				arr.add(statistic);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return arr;
	}

	/**
	 * 연대별 비율 데이터
	 * 
	 * @param uidx
	 * @return
	 */
	public ArrayList<DateYearRanking> getDatePercent(int uidx) {
		this.Clear();
		ArrayList<DateYearRanking> arr = null;
		int sumlistcount = 0;
		ResultSet rs = null;
		try {
			DateYearRanking statistic = null;
			sql = "SELECT m.mdate_year, count(mdate_year) as count FROM rating as r "
					+ "inner join music as m on m.midx=r.rmidx "
					+ "where r.ruidx = ? and r.rmusic_rating >? group by m.mdate_year "
					+ "order by count desc";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, uidx);
			ps.setFloat(2, FavoriteSpliter.MIN_RATING);
			rs = ps.executeQuery();
			arr = new ArrayList<DateYearRanking>();

			while (rs.next()) {
				statistic = new DateYearRanking(rs.getInt(1), rs.getInt(2));
				sumlistcount += rs.getInt(2);
				arr.add(statistic);
			}
			// sum array 에 넣어서
			statistic = new DateYearRanking(-1, sumlistcount);
			arr.add(statistic);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return arr;
	}

	/**
	 * 가수 TOP5를 가져오는 로직
	 * 
	 * @param uidx
	 *            - 유저 아이디
	 * @return null 또는 SingerStatistic 객체를 담은 리스트
	 */
	public ArrayList<SingerStatistic> getArtistRank(int uidx) {
		this.Clear();
		ArrayList<SingerStatistic> arr = null;
		ResultSet rs = null;
		try {
			SingerStatistic statistic = null;
			sql = "SELECT m.martist, count(martist) as count,m.midx FROM rating as r "
					+ "inner join music as m on m.midx=r.rmidx "
					+ "where r.ruidx = ? and r.rmusic_rating > ? group by martist "
					+ "order by count desc limit 5";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, uidx);
			ps.setFloat(2, FavoriteSpliter.MIN_RATING);
			rs = ps.executeQuery();
			arr = new ArrayList<SingerStatistic>();
			while (rs.next()) {
				statistic = new SingerStatistic(rs.getString(1), rs.getInt(2));
				arr.add(statistic);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return arr;
	}

	/**
	 * 추천 테이블에서 가져온 정보를 사용자에게 제공한다. 메인 페이지에 필요한 상세 점보 및 간략 정보를 처리하는 클래스
	 * 
	 * @param uindex
	 *            - 사용자 인덱스
	 * @return - 결과값 (0번째 인덱스는 상세 정보가 저장 1~... 간략 정보 저장)
	 */
	public ArrayList<SimpleMusic> getMainPageMusicItems(
			List<UserFavoriteData> favoriteList) {
		this.Clear();
		ArrayList<SimpleMusic> musicList = null;
		List<UserFavoriteData> subFavoriteList = null;
		ResultSet rs = null;
		try {
			DetailMusic detailMusic = null;
			UserFavoriteData userFavorite = favoriteList.get(0);
			sql = "SELECT m.martist, m.mtitle, m.mgenre, a.atitle, a.aimg_path, a.adate "
					+ "from music as m inner join album as a on a.aidx = m.maidx where m.midx = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userFavorite.getMidx());
			rs = ps.executeQuery();

			musicList = new ArrayList<SimpleMusic>();
			if (rs.next()) {
				detailMusic = new DetailMusic(userFavorite.getMidx(),
						rs.getString(1), rs.getString(2), rs.getString(5),
						userFavorite.getRating(), rs.getDate(6).toString(),
						rs.getString(3), rs.getString(4));
				musicList.add(detailMusic);
			}

			subFavoriteList = favoriteList.subList(1, favoriteList.size());

			getMainPageSimpleItem(subFavoriteList, musicList);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return musicList;
	}

	public String getCurrentCSV() {
		// Statement 초기화
		this.Clear();

		// ResultSet 선언
		ResultSet rs = null;

		// csv file path
		String pathCSV = null;

		try {
			sql = "SELECT rmCurrentFileName FROM Recommend LIMIT 1;";

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				pathCSV = rs.getString("rmCurrentFileName");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return pathCSV;
	}

	public void setCurrentCSV(String filepath) {
		// Statement 초기화
		this.Clear();

		// 파라미터 값 검증
		if (filepath == null || filepath.equals("")) {
			return;
		}

		try {

			sql = "UPDATE Recommend SET rmCurrentFileName = ?;";

			ps = conn.prepareStatement(sql);
			ps.setString(1, filepath);
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getCurrentKN1() {
		// Statement 초기화
		this.Clear();

		// ResultSet 선언
		ResultSet rs = null;

		// csv file path
		String pathCSV = null;

		try {
			sql = "SELECT kfileName FROM Kn1 LIMIT 1;";

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				pathCSV = rs.getString("kfileName");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return pathCSV;
	}

	public void setCurrentKN1(String filepath) {
		// Statement 초기화
		this.Clear();

		// 파라미터 값 검증
		if (filepath == null || filepath.equals("")) {
			return;
		}

		try {

			sql = "UPDATE Kn1 SET kfileName = ?;";

			ps = conn.prepareStatement(sql);
			ps.setString(1, filepath);
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setSurveyValue(int uidx,int value) {
		// Statement 초기화
		this.Clear();

		try {
			sql = "UPDATE user SET usurvey = ? where uidx = ?;";

			ps = conn.prepareStatement(sql);
			ps.setInt(1, value);
			ps.setInt(2, uidx);
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<SimpleMusic> getMainPageSimpleItem(
			List<UserFavoriteData> favoriteList,
			ArrayList<SimpleMusic> musicList) {
		this.Clear();
		StringBuffer notInBuffer = null;
		ResultSet rs = null;
		if (favoriteList != null) {
			notInBuffer = new StringBuffer();
			notInBuffer
					.append("select m.midx, m.martist, m.mtitle, a.aimg_path "
							+ "from album as a join music as m on m.maidx = a.aidx "
							+ "where m.midx in (");
			for (UserFavoriteData userFavorite : favoriteList) {
				notInBuffer.append(userFavorite.getMidx());
				notInBuffer.append(",");
			}
			int bufsize = notInBuffer.length();
			if (bufsize != 0) {
				notInBuffer.setCharAt(bufsize - 1, ' ');
			}
			notInBuffer.append(")");
			try {
				sql = notInBuffer.toString();
				// System.out.println(sql);
				ps = conn.prepareStatement(sql,
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);
				rs = ps.executeQuery();
				int rsSize = getResultSetSize(rs);
				int countRow = 0;
				if (rsSize == favoriteList.size()) {
					while (rs.next()) {
						musicList.add(new SimpleMusic(rs.getInt(1), rs
								.getString(2), rs.getString(3),
								rs.getString(4), favoriteList.get(countRow)
										.getRating()));
						countRow++;
					}
				}

				// simpleMusic = new SimpleMusic(midx, artist, title, imagePath,
				// rating)
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return musicList;

	}

	/**
	 * PrepareStatement에 ResultSet.TYPE_SCROLL_INSENSITIVE,
	 * ResultSet.CONCUR_READ_ONLY를 설정했을때만 사용할수있다.
	 * 
	 * @param resultSet
	 * @return
	 */
	public static int getResultSetSize(ResultSet resultSet) {
		int size = -1;

		try {
			resultSet.last();
			size = resultSet.getRow();
			resultSet.beforeFirst();
		} catch (SQLException e) {
			return size;
		}

		return size;
	}

	/**
	 * 사용자가 매긴 rating갯수 주는클래스
	 * 
	 * @param uindex
	 * @return coutnraing
	 */
	public long getCountRating(Integer uindex) {
		this.Clear();
		long result = 0;
		if (uindex < 0) {
			return 0;
		}
		ResultSet rs = null;
		try {
			sql = "select count(1) AS rowcount from rating where ruidx = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, uindex);
			rs = ps.executeQuery();
			rs.next();
			result = rs.getInt("rowcount");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public boolean existAlbum(int uidx, int albumidx) {
		this.Clear();
		boolean success = false;
		// 내앨범 맞는지
		try {
			sql = "select * from playlist where puidx = ? and pidx = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, uidx);
			ps.setInt(2, albumidx);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				success = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		return success;
	}

	/**
	 * 플레이 리스트에 추가할 경우 레이팅 테이블 갱신
	 * 
	 * @param uidx
	 *            - 사용자 인덱스
	 * @param midx
	 *            - 음악 인덱스
	 * @return
	 */
	public boolean updateIsPlayList(int uidx, long midx) {
		this.Clear();
		boolean success = false;
		ResultSet rs = null;
		try {
			sql = "select count(*) from rating where rating.ruidx = ? and rmidx = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, uidx);
			ps.setLong(2, midx);
			rs = ps.executeQuery();
			rs.next();
			if (rs.getInt(1) != 0) {
				rs.close();
				// System.out.println("update");
				sql = "update rating set risplaylist = 1 where rating.ruidx = ? and rating.rmidx = ?";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, uidx);
				ps.setLong(2, midx);
				ps.executeUpdate();
				success = true;

			} else {
				rs.close();
				// System.out.println("insert");
				sql = "insert into rating(rating.ruidx,rating.rmidx,rating.risplaylist) values(?,?,1)";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, uidx);
				ps.setLong(2, midx);
				ps.executeUpdate();
				success = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return success;
	}

	public static final int PLAYLIST_MUSIC_COUNT = 30;

	/**
	 * 플레이 리스트에 담긴 음악의 개수
	 * 
	 * @param playlistIndex
	 *            - 플레이 리스트 인덱스
	 * @return
	 */
	public int getPlaylistMusicCount(int playlistIndex) {
		this.Clear();
		int musicCount = 0;
		ResultSet rs = null;
		try {
			sql = "select count(*) from item where item.ipidx=?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, playlistIndex);
			rs = ps.executeQuery();
			rs.next();
			musicCount = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return musicCount;
	}

	/**
	 * 플레이 리스트에 음악 추가
	 * 
	 * @param uidx
	 * @param albumidx
	 *            - 앨범 인덱스
	 * @param musiclist
	 *            - 음악 리스트(midx)
	 * @return
	 */
	public boolean insertMusicToPlayList(int uidx, int albumidx,
			ArrayList<Long> musiclist) {
		this.Clear();
		boolean success = false;
		// 내앨범 맞는지
		if (existAlbum(uidx, albumidx) == true) {
			for (Long midx : musiclist) {
				try {
					conn.setAutoCommit(false);
					sql = "insert into item(ipidx,imidx) values(?,?)";
					ps = conn.prepareStatement(sql);
					ps.setInt(1, albumidx);
					ps.setLong(2, midx);
					ps.executeUpdate();

					if (updateIsPlayList(uidx, midx))
						conn.commit();
					success = true;
				} catch (Exception e) {
					e.printStackTrace();
					success = false;
				} finally {
					try {
						conn.setAutoCommit(true);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					this.Clear();
				}
			}
		}
		return success;
	}

	public boolean setRatings(ArrayList<SimpleMusic> songrating, int uindex) {
		this.Clear();
		boolean success = false;
		if (songrating == null || songrating.size() == 0) { // 객체 널 체크
			return false;
		}
		for (int i = 0; i < songrating.size(); i++) {
			try {
				if (hasRating(uindex, songrating.get(i).getMidx())) {
					this.Clear();
					sql = "update rating set rmusic_rating=?,rmusic_prerating=? where ruidx = ? and rmidx = ?";
					System.out.println("update rating : " + uindex + " "
							+ songrating.get(i).getMidx() + " "
							+ songrating.get(i).getRating());
					ps = conn.prepareStatement(sql);
					ps.setFloat(1, songrating.get(i).getRating());
					ps.setFloat(2, songrating.get(i).getPredictrating());
					ps.setInt(3, uindex);
					ps.setInt(4, songrating.get(i).getMidx());
					ps.executeUpdate();
					success = true;
				} else {
					this.Clear();
					sql = "insert into rating (ruidx, rmusic_prerating, rmidx, rmusic_rating) values (?,?,?,?);";
					ps = conn.prepareStatement(sql);
					System.out.println("insert rating : " + uindex + " "
							+ songrating.get(i).getMidx() + " "
							+ songrating.get(i).getRating());
					ps.setInt(1, uindex);
					ps.setFloat(2, songrating.get(i).getPredictrating());
					ps.setInt(3, songrating.get(i).getMidx());
					ps.setFloat(4, songrating.get(i).getRating());
					ps.executeUpdate();
					success = true;
				}
			} catch (SQLException e) {
				System.out.println(e.toString());
			} finally {
				this.Clear();
			}
		}
		return success;
	}

	/**
	 * 음악 컨텐츠를 제외한 플레이 리스트 인덱스
	 * 
	 * @param uidx
	 * @return
	 */
	public ArrayList<Playlist> getPlaylistNotContainMusic(int uidx) {
		// Statement 초기화
		this.Clear();

		// ResultSet 선언
		ResultSet rs = null;

		// 플레이리스트
		ArrayList<Playlist> PlaylistResult = null;

		try {

			sql = "SELECT pidx, ptitle FROM SSong.playlist WHERE puidx = ?;";

			ps = conn.prepareStatement(sql);

			ps.setInt(1, uidx);
			rs = ps.executeQuery();
			PlaylistResult = new ArrayList<Playlist>();
			while (rs.next()) {
				Playlist plist = new Playlist();

				plist.setListidx(rs.getInt("pidx"));
				plist.setTitle(rs.getString("ptitle"));
				// plist.setSongcount(0); //임시

				PlaylistResult.add(plist);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return PlaylistResult;
	}

	/**
	 * 플레이 카운트 업데이트
	 * 
	 * @param uidx
	 * @param midx
	 * @return
	 */
	public boolean updatePlayCount(int uidx, int midx) {
		this.Clear();
		boolean success = false;
		ResultSet rs = null;
		try {
			sql = "select count(*) from rating where rating.ruidx = ? and rmidx = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, uidx);
			ps.setInt(2, midx);
			rs = ps.executeQuery();
			rs.next();
			if (rs.getInt(1) != 0) {
				rs.close();
				// System.out.println("update");
				sql = "update rating set rplay_count = rplay_count+1 where rating.ruidx = ? and rating.rmidx = ?";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, uidx);
				ps.setInt(2, midx);
				int updateResult = ps.executeUpdate();
				success = true;

			} else {
				rs.close();
				// System.out.println("insert");
				sql = "insert into rating(rating.ruidx,rating.rmidx,rating.rplay_count) values(?,?,1)";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, uidx);
				ps.setInt(2, midx);
				int updateResult = ps.executeUpdate();
				success = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return success;
	}

	public ArrayList<Playlist> getPlaylist(int uidx) {
		// Statement 초기화
		this.Clear();

		// ResultSet 선언
		ResultSet rs = null;

		// 플레이리스트
		ArrayList<Playlist> PlaylistResult = new ArrayList<Playlist>();

		try {

			sql = "SELECT pidx, ptitle FROM SSong.playlist WHERE puidx = ?;";

			ps = conn.prepareStatement(sql);

			ps.setInt(1, uidx);
			rs = ps.executeQuery();

			while (rs.next()) {
				Playlist plist = new Playlist();

				plist.setListidx(rs.getInt("pidx"));
				plist.setTitle(rs.getString("ptitle"));
				// plist.setSongcount(0); //임시

				PlaylistResult.add(plist);
			}

			for (Playlist item : PlaylistResult) {
				item.setSongs(getPlaylistSongs(item.getListidx()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return PlaylistResult;
	}

	public ArrayList<ListMusic> getPlaylistSongs(int playlistidx) {
		// Statement 초기화
		this.Clear();

		// ResultSet 선언
		ResultSet rs = null;

		// 플레이리스트
		ArrayList<ListMusic> SongsResult = new ArrayList<ListMusic>();

		try {

			sql = "SELECT item.iidx, music.midx, music.martist, music.mtitle, music.mgenre FROM item INNER JOIN music ON (item.imidx = music.midx) WHERE ipidx = ?;";

			ps = conn.prepareStatement(sql);

			ps.setInt(1, playlistidx);
			rs = ps.executeQuery();

			while (rs.next()) {

				ListMusic music = new ListMusic(rs.getInt("item.iidx"),
						rs.getInt("music.midx"), rs.getString("music.martist"),
						rs.getString("music.mtitle"),
						rs.getString("music.mgenre"));

				SongsResult.add(music);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return SongsResult;
	}

	public boolean isExistPlaylist(int uidx, String playlist) {
		// Statement 초기화
		this.Clear();

		// ResultSet 선언
		ResultSet rs = null;

		// 결과
		boolean ExistResult = false;

		// 파라미터 값 검증
		if (playlist == null || playlist.equals("")) {
			return false;
		}

		try {

			sql = "SELECT pidx FROM playlist WHERE puidx=? and ptitle=? LIMIT 1;";

			ps = conn.prepareStatement(sql);

			ps.setInt(1, uidx);
			ps.setString(2, playlist);

			rs = ps.executeQuery();

			if (rs.next()) {
				ExistResult = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ExistResult;
	}

	public boolean isExistPlaylist(int uidx, int pidx) {
		// Statement 초기화
		this.Clear();

		// ResultSet 선언
		ResultSet rs = null;

		// 결과
		boolean ExistResult = false;

		try {

			sql = "SELECT ptitle FROM playlist WHERE puidx=? and pidx=? LIMIT 1;";

			ps = conn.prepareStatement(sql);

			ps.setInt(1, uidx);
			ps.setInt(2, pidx);

			rs = ps.executeQuery();

			if (rs.next()) {
				if (rs.getString("ptitle").equals("기본리스트")) {
					ExistResult = false;
				} else {
					ExistResult = true;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ExistResult;
	}

	public void addPlaylist(int uidx, String playlist) {
		this.Clear();
		// 파라미터 값 검증
		if (playlist == null || playlist.equals("")) {
			return;
		}

		if (isExistPlaylist(uidx, playlist))
			return; // 이미 같은 이름이 존재
		// Statement 초기화
		this.Clear();

		try {

			sql = "INSERT INTO playlist (ptitle, puidx) VALUES (?, ?);";

			ps = conn.prepareStatement(sql);

			ps.setString(1, playlist);
			ps.setInt(2, uidx);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public long getHearSongCount(Integer uindex) {
		this.Clear();
		long result = 0;
		if (uindex < 0) {
			return 0;
		}
		ResultSet rs = null;
		try {
			sql = "SELECT count(1) as songcount FROM rating as r where r.ruidx = ? and r.rplay_count > 0;";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, uindex);
			rs = ps.executeQuery();
			rs.next();
			result = rs.getInt("songcount");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public void removePlaylist(int uidx, int pidx) {

		if (!isExistPlaylist(uidx, pidx))
			return; // 없다그런거

		// Statement 초기화
		this.Clear();

		try {
			System.out.println("playlist : " + pidx);
			sql = "DELETE FROM playlist WHERE pidx=?;";

			ps = conn.prepareStatement(sql);

			ps.setInt(1, pidx);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void editPlaylist(int uidx, int pidx, String playlist) {

		// 파라미터 값 검증
		if (playlist == null || playlist.equals("") || playlist.equals("기본리스트")) {
			return;
		}

		if (!isExistPlaylist(uidx, pidx))
			return; // 없다그런거

		// Statement 초기화
		this.Clear();

		try {

			sql = "UPDATE playlist SET ptitle=? WHERE pidx=?;";

			ps = conn.prepareStatement(sql);

			ps.setString(1, playlist);
			ps.setInt(2, pidx);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public int getPlaylistCount(int uidx) {
		// Statement 초기화
		this.Clear();

		// ResultSet 선언
		ResultSet rs = null;

		// 결과
		int CountResult = 0;

		try {

			sql = "SELECT count(pidx) as cnt FROM SSong.playlist WHERE puidx=?;";

			ps = conn.prepareStatement(sql);

			ps.setInt(1, uidx);

			rs = ps.executeQuery();

			if (rs.next()) {
				CountResult = rs.getInt("cnt");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return CountResult;
	}

	public boolean isExistItem(int uidx, int iidx) {
		// Statement 초기화
		this.Clear();

		// ResultSet 선언
		ResultSet rs = null;

		// 결과
		boolean ExistResult = false;

		try {

			sql = "SELECT item.iidx FROM item INNER JOIN playlist ON (item.ipidx = playlist.pidx) WHERE playlist.puidx=? and item.iidx=? LIMIT 1;";

			ps = conn.prepareStatement(sql);

			ps.setInt(1, uidx);
			ps.setInt(2, iidx);

			rs = ps.executeQuery();

			if (rs.next()) {
				ExistResult = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ExistResult;
	}

	public void removeItem(int uidx, int iidx) {

		if (!isExistItem(uidx, iidx))
			return; // 없다그런거

		// Statement 초기화
		this.Clear();

		try {

			setItemPlaylistCount(uidx, iidx, 0);
			this.Clear();
			sql = "DELETE FROM item WHERE iidx=?;";

			ps = conn.prepareStatement(sql);

			ps.setInt(1, iidx);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setItemPlaylistCount(int uidx, int iidx, int value) {
		// Statement 초기화
		this.Clear();

		try {

			System.out.println("서버에서 삭제해줘" + uidx + ":" + iidx + ":" + value);

			sql = "UPDATE rating INNER JOIN item ON (item.imidx=rating.rmidx) SET rating.risplaylist=? WHERE rating.ruidx=? AND item.iidx=?;";

			ps = conn.prepareStatement(sql);

			ps.setInt(1, value);
			ps.setInt(2, uidx);
			ps.setInt(3, iidx);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<SimpleMusic> getRatedMusicItems(int uidx) {

		this.Clear();

		ArrayList<SimpleMusic> musicList = new ArrayList<SimpleMusic>();

		ResultSet rs = null;

		try {

			sql = "SELECT music.midx, music.martist, music.mtitle, album.aimg_path, rating.rmusic_rating, album.adate, music.mgenre, album.atitle "
					+ "FROM music "
					+ "INNER JOIN album "
					+ "ON album.aidx = music.maidx "
					+ "INNER JOIN rating "
					+ "ON music.midx = rating.rmidx "
					+ "WHERE rating.ruidx=? AND rating.rmusic_rating > 0;";

			ps = conn.prepareStatement(sql);

			ps.setInt(1, uidx);

			rs = ps.executeQuery();

			while (rs.next()) {
				DetailMusic detailMusic = new DetailMusic(
						rs.getInt("music.midx"), rs.getString("music.martist"),
						rs.getString("music.mtitle"),
						rs.getString("album.aimg_path"),
						rs.getFloat("rating.rmusic_rating"), rs.getDate(
								"album.adate").toString(),
						rs.getString("music.mgenre"),
						rs.getString("album.atitle"));
				musicList.add(detailMusic);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return musicList;
	}

	public long getListSongCount(Integer uindex) {
		long result = 0;
		if (uindex < 0) {
			return 0;
		}
		ResultSet rs = null;
		try {
			sql = "SELECT DISTINCT count(1) as songcount FROM item INNER JOIN music ON (item.imidx = music.midx) WHERE ipidx in (Select pidx from playlist as p where p.puidx = ?);";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, uindex);
			rs = ps.executeQuery();
			rs.next();
			result = rs.getInt("songcount");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;

	}

	public Poem getPoem() {

		// Statement 초기화
		this.Clear();

		// ResultSet 선언
		ResultSet rs = null;

		// 기타 변수들 선언
		Poem PoemResult = new Poem();

		try {

			sql = "SELECT writter, title, poemdata FROM SSong.poem ORDER BY rand() LIMIT 1;";

			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			if (rs.next()) {
				PoemResult.setWritter(rs.getString("writter"));
				PoemResult.setTitle(rs.getString("title"));
				PoemResult.setPoemdata(rs.getString("poemdata"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return PoemResult;
	}

	/*
	 * 취향 분석 페이지에 선호곡 보여주는것
	 */
	public ArrayList<SimpleMusic> getSongsRank(Integer uindex) {
		this.Clear();
		ArrayList<SimpleMusic> arr = null;
		ResultSet rs = null;
		try {
			
			SimpleMusic song = null;
			sql = "SELECT m.midx, m.martist, m.mtitle, (r.rmusic_rating*0.85 + r.risplaylist*0.05 + r.rplay_count*0.1) "
					+ "as rankcount FROM rating as r inner join music as m on m.midx=r.rmidx "
					+ "where r.ruidx = ? and r.rmusic_rating > ? group by m.midx "
					+ "order by rankcount desc limit 5;";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, uindex);
			ps.setFloat(2, FavoriteSpliter.MIN_RATING);
			rs = ps.executeQuery();
			arr = new ArrayList<SimpleMusic>();
			while (rs.next()) {
				song = new SimpleMusic(rs.getInt(1), rs.getString(2),rs.getString(3),
						"",rs.getFloat(4));
				arr.add(song);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return arr;
	}
	/**
	 * 사용자 index 있는지 체크
	 * 
	 * @param uidx
	 * @return
	 */
	public boolean getUindexExist(int uidx) {
		this.Clear();
		boolean success = false;
		ResultSet rs = null;
		try {
			sql = "SELECT count(1) FROM user WHERE uidx=?;";
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setInt(1, uidx);
			rs = ps.executeQuery();

			if (rs.next()) {
				success = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return success;
	}

	public String getUserNickname(int favoriteuidx) {
		this.Clear();
		String nickname ="";
		ResultSet rs = null;
		try {
			
			sql = "Select user.unick from user where user.uidx = ?;";
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setInt(1, favoriteuidx);
			rs = ps.executeQuery();

			if (rs.next()) {
				nickname = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return nickname;
	}
}