package com.ssong.listener;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.ssong.db.ResourceConstants;


/**
 * Application Lifecycle Listener implementation class SSongServletContextListener
 *
 */
@WebListener
public class SSongServletContextListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public SSongServletContextListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
    }
	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0) {
    	ResourceConstants.Favorite = arg0.getServletContext().getRealPath("/csv");
		File favoriteFile = new File(ResourceConstants.Favorite);
		
		if(!favoriteFile.exists()){
			favoriteFile.mkdirs();
		}
    }
	
}
