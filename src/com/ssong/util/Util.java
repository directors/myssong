package com.ssong.util;

import java.util.regex.Pattern;

public class Util {
	public static String GetEncodeByUtf8(String strValue) throws Exception{
		try {
			if(null != strValue)
				if(Pattern.matches("^[ㄱ-ㅎ-ㅏ-ㅣ-가-힣-a-zA-Z0-9-\\x00-\\x7F]*$", strValue) == false)
					strValue = new String(strValue.getBytes("8859_1"), "UTF-8");
				
		} catch (Exception e) {
			throw e;
		}
		return strValue;
	}
}
