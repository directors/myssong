package com.ssong.file;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.ssong.bean.UserFavoriteData;
import com.ssong.bean.UserFavoriteUser;
import com.ssong.db.ResourceConstants;

public class FavoriteSpliter {
	
	/**
	 * 유사한 사용자를 찾는 함수
	 * @param filename
	 * @param uidx
	 * @return
	 */
	public static final UserFavoriteUser getUserFavoriteUser(String filename, int uidx){
		//스트림 생성
		UserFavoriteUser user= null;
		FileReader csvInputStream = null;
		BufferedReader csvbr = null;
		try{
			csvInputStream = new FileReader(ResourceConstants.Favorite+"/"+filename);
			csvbr = new BufferedReader(csvInputStream);
			//검색할 user 인덱스 준비
			String searchIndex = Integer.toString(uidx);
			byte[] searchIndexByBytes = searchIndex.getBytes();
			int searchIndexByBytesLength = searchIndexByBytes.length;
			//매번 생성할 필요 없는 지역 변수
			String lineStr=null;
			int forIndex = 0;
			int splitIndex=-1;
			byte[] targetIndexByBytes = null;
			
			while(((lineStr = csvbr.readLine())!=null)){
				splitIndex = lineStr.indexOf(',');
				targetIndexByBytes = lineStr.getBytes();
				if(splitIndex == searchIndexByBytesLength){	/*사용자 인덱스 문자열 길이와 ','의 인덱스가 같을때만 동작*/
					for(forIndex=0; forIndex<splitIndex; forIndex++){
						if(searchIndexByBytes[forIndex] != targetIndexByBytes[forIndex]){
							break; //존재하지 않는다 실패
						}else{
							//유사한 사용자가 존재한다.
							if(forIndex ==  splitIndex-1){
								user = parseUserFavoriteUser(lineStr);
								if(user!=null){
									return user;
								}
							}
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(csvbr!=null){
				try {
					csvbr.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println("유사 사용자 csv에 사용자 데이터가 존재하지 않습니다.");
		return null;//유사 사용자 데이터가 존재하지 않는다.
	}
	/**
	 * /**
	 * 사용자의 선호 정보를 가져오는 메서드
	 * @param filename - 파일이
	 * @param uidx - 사용자 인덱스
	 * @param startOffset - 시작 오프셋 [0 ~ ...]의 범위를 가짐
	 * @param count - 가져올 개수
	 * @return
	 */
	public static final ArrayList<UserFavoriteData> getUserFavoriteList(String filename, int uidx
			,int startOffset, int count){
		//스트림 생성
		ArrayList<UserFavoriteData> favoriteList= null;
		FileReader csvInputStream = null;
		BufferedReader csvbr = null;
		try{
			csvInputStream = new FileReader(ResourceConstants.Favorite+"/"+filename);
			csvbr = new BufferedReader(csvInputStream);
			//검색할 user 인덱스 준비
			String searchIndex = Integer.toString(uidx);
			byte[] searchIndexByBytes = searchIndex.getBytes();
			int searchIndexByBytesLength = searchIndexByBytes.length;
			//매번 생성할 필요 없는 지역 변수
			String lineStr=null;
			int forIndex = 0;
			int splitIndex=-1;
			byte[] targetIndexByBytes = null;
			//첫번째 user index을 찾아서 시작/종료를 알리는 플래그 
			boolean startFlag=false;
			boolean endFlag=false;
			int offsetCount=0;	//현재 탐색 레코드 개수를 담는 변수[0 ~ ...]
			//추천 데이터
			UserFavoriteData recommendData=null;
			//결과를 담을 리스트 생성
			favoriteList = new ArrayList<UserFavoriteData>();
			/**
			 * 라인단위로 탐색 알고리즘
			 * uidx가 줄 연속적으로 있을 경우만 고려한다.
			 * 모든 줄에 대해 subString후 --> parseInt롤 이용하지 않고
			 * byte단위 비교를 수행
			 */
			while(((lineStr = csvbr.readLine())!=null) && startOffset>=0 && count>=1){
				splitIndex = lineStr.indexOf(',');
				targetIndexByBytes = lineStr.getBytes();
				if(splitIndex == searchIndexByBytesLength){	/*사용자 인덱스 문자열 길이와 ','의 인덱스가 같을때만 동작*/
					for(forIndex=0; forIndex<splitIndex; forIndex++){
						if(searchIndexByBytes[forIndex] != targetIndexByBytes[forIndex]){
							if(startFlag==true){	//시작 플래그가 올린 상태에서 비교한 uidx 가 다르다면 
								//마지막까지 탐색이 끝났다 뜻으로 종료 플래그를 올린다.
								endFlag = true;
							}
							break; //실패
						}else{
							if(forIndex == splitIndex-1){ //마지막 인덱스 까지 같다면
								if(startFlag == false)	//시작 플래그를 올린다.
									startFlag = true;
								if(offsetCount >= startOffset){
									//인스턴스를 메모리에 저장
									recommendData = FavoriteSpliter.parseUserFavoriteData(lineStr);
									if(recommendData!=null){
										favoriteList.add(FavoriteSpliter.parseUserFavoriteData(lineStr));
									}
								}
								/**
								 * 현재 탐색한 레코드 개수
								 */
								offsetCount++;
								/**
								 * 탐색한 레코드 개수 - 시작 오프셋 = 메모리로 읽은 개수
								 * 현재 읽은 레코드의 개수가 카운트이면 끝낸다.
								 * if문에 있는 endflag=true 조건과 다른 용도이다. Count를 세어
								 * 그 Count와 같을때 적용되는 종료조건.
								 */
								if(offsetCount - startOffset >= count){
									endFlag = true;	
								}
							}
						}
					}
					if(startFlag=true && endFlag==true){	//시작과 종료가 모두 올라와서 uidx 탐색을 종료한다.
						break;
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(csvbr!=null){
				try {
					csvbr.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return favoriteList;
	}
	
	public static final float MIN_RATING = 2.5f;
	/**
	 * 사용자 추천 아이템 데이터를 파싱하여 인스턴스화 하는 메서드
	 * @param userDataStr
	 * @return
	 */
	public static final UserFavoriteData parseUserFavoriteData(String userDataStr){
		//8003,377,5.000000와 같은 포맷을 지닌 스트링
		UserFavoriteData userFavorite = null;
		String[] favoriteArr= userDataStr.split(",");
		int length = favoriteArr.length;//하지만 항상 3임을 보장 
		if(length >= 3){
			userFavorite = new UserFavoriteData();
			for(int i=0; i<length; i++){
				switch(i){
				case 0:{	//사용자 idx
					userFavorite.setUidx(Integer.parseInt(favoriteArr[i]));
					break;
				}
				case 1:{	//음악 idx
					userFavorite.setMidx(Integer.parseInt(favoriteArr[i]));
					break;
				}
				case 2:{	//선호점수 idx
					float rating = Float.parseFloat(favoriteArr[i]);
					userFavorite.setRating(rating);
					if(rating<MIN_RATING){
						return null;
					}
					break;
				}
				}
			}
		}
		return userFavorite;
	}
	
	/**
	 * 사용자와 가장 유사한 유저를 찾는 함수
	 * @param userDataStr
	 * @return
	 */
	public static final UserFavoriteUser parseUserFavoriteUser(String userDataStr){
		//8003,377,5.000000와 같은 포맷을 지닌 스트링
		UserFavoriteUser userFavoriteuser = null;
		String[] favoriteArr= userDataStr.split(",");
		int length = favoriteArr.length;//하지만 항상 3임을 보장 
		try{
			if(length >= 3){
				userFavoriteuser = new UserFavoriteUser();
				for(int i=0; i<length; i++){
					switch(i){
					case 1:{	//사용자 인덱스
						userFavoriteuser.setFavoriteuidx(Integer.parseInt(favoriteArr[i]));
						break;
					}
					case 2:{	//유사도
						float rating = Float.parseFloat(favoriteArr[i]);
						userFavoriteuser.setFavoritescore(rating);
						break;
					}
					}
				}
			}
		}catch(Exception e){
			System.out.println("유사 사용자 csv에 옯바르지 않은 형식이 포함되어 있습니다.");
			userFavoriteuser= null;
		}
		return userFavoriteuser;
	}
}
