package com.ssong.json;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.ssong.bean.SimpleMusic;

public class JsonMaker {

	public static ArrayList<Long> parseMusicIndexArray(String src){
		JSONArray musicList = null;
		ArrayList<Long> result = null;
		try{
			musicList = (JSONArray) JSONValue.parse(src);
			if(musicList!=null){
				System.out.println(musicList+":"+musicList.size());
				result = new ArrayList<Long>();
				for(int i = 0 ; i < musicList.size() ; i ++){
					result.add((Long)musicList.get(i));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
		}
		return result;
	}
	
	public static ArrayList<SimpleMusic> parseArrayJson(String src) {
		ArrayList<SimpleMusic> ratingMusics = new ArrayList<SimpleMusic>();
		JSONArray ratinglist = null;
		int oneDecimal;
		float roundres = 0;
		try {
			ratinglist = (JSONArray) JSONValue.parse(src);
			for (int i = 0; i < ratinglist.size(); i++) {
				JSONObject rating = new JSONObject();
				rating = (JSONObject) ratinglist.get(i);
				// 0.5단위로 반올림
				double score = Double.parseDouble(rating.get("rating")
						.toString());
				DecimalFormat newFormat = new DecimalFormat("#.##");
				double twoDecimal = Double.valueOf(newFormat.format(score));
				double oneround = (twoDecimal * 100) % 100;
				if (oneround >= 75)
					roundres = (int) twoDecimal + 1;
				else if (oneround >= 25)
					roundres = (float) ((int) twoDecimal + 0.5);
				else
					roundres = (int) twoDecimal;
				if (roundres == 0)// 0rating은없음
					roundres = 1;

			//	System.out.println(roundres + " [score] " + score + " [realscore] ");

				SimpleMusic music = new SimpleMusic(Integer.parseInt(rating
						.get("midx").toString()), null, null, null, roundres);
				
				ratingMusics.add(music);
			}

		} catch (Exception e) {
			System.out.println(e.toString()+":파싱 에러");
			return null;
		}

		return ratingMusics;
	}

	public static String getCode(boolean result) {
		JSONObject obj = null;
		obj = new JSONObject();
		obj.put("result", result);
		return obj.toJSONString();
	}

}
