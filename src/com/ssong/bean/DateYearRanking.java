package com.ssong.bean;

import java.beans.Beans;

public class DateYearRanking extends Beans {
	
	public DateYearRanking(int dateYear, int dateCount) {
		super();
		this.dateYear = dateYear;
		this.dateCount = dateCount;
	}
	public int getDateYear() {
		return dateYear;
	}
	public void setDateYear(int dateYear) {
		this.dateYear = dateYear;
	}
	public int getDateCount() {
		return dateCount;
	}
	public void setDateCount(int dateCount) {
		this.dateCount = dateCount;
	}
	int dateYear;
	int dateCount;
}
