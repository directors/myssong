package com.ssong.bean;

import java.beans.Beans;

public class Poem extends Beans {

	String writter;
	String title;
	String poemdata;
	
	public String getWritter() {
		return writter;
	}
	public void setWritter(String writter) {
		this.writter = writter;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPoemdata() {
		return poemdata;
	}
	public void setPoemdata(String poemdata) {
		this.poemdata = poemdata;
	}

}
