package com.ssong.bean;

import java.beans.Beans;

/**
 * 장르 통계를 위한 클래스
 * @author changwoncheo
 *
 */
public class GenreStatistic extends Beans {
	String genre;
	int count;
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public GenreStatistic(String genre, int count) {
		super();
		this.genre = genre;
		this.count = count;
	}
	@Override
	public String toString() {
		return "GenreStatistic [genre=" + genre + ", count=" + count + "]";
	}
}
