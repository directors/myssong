/**
 * 
 */
package com.ssong.bean;

import java.beans.Beans;

/**
 * @author changwoncheo 간략한 음악
 */
public class SimpleMusic extends Beans {
	String imagePath;
	String title;
	String artist;
	int artistidx;
	public int getArtistidx() {
		return artistidx;
	}

	public void setArtistidx(int artistidx) {
		this.artistidx = artistidx;
	}

	float rating;
	int midx;

	float predictrating;
	/**
	 * 상세페이지가 아닌 간략한 목록형태로 주어질때 필요한 객체
	 * 
	 * @param midx
	 *            - 음악 인덱스
	 * @param artist
	 *            - 가수
	 * @param title
	 *            - 제목
	 * @param imagePath
	 *            - 이미지 경로
	 * @param score
	 *            - 점수
	 */
	public SimpleMusic(int midx, String artist, String title,  String imagePath,float rating ){
		this.midx = midx;
		this.imagePath = imagePath;
		this.title = title;
		this.artist = artist;
		this.rating = rating;
		
	}

	public float getPredictrating() {
		return predictrating;
	}

	public void setPredictrating(float predictrating) {
		this.predictrating = predictrating;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}

	public int getMidx() {
		return midx;
	}

	public void setMidx(int midx) {
		this.midx = midx;
	}

	@Override
	public String toString() {
		return "SimpleMusic [imagePath=" + imagePath + ", title=" + title
				+ ", artist=" + artist + ", artistidx=" + artistidx
				+ ", rating=" + rating + ", midx=" + midx + ", predictrating="
				+ predictrating + "]";
	}
}
