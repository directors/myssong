package com.ssong.bean;

import java.beans.Beans;

/**
 * 사용자 등록 정보를 저장하는 클래스
 * @author changwoncheo
 *
 */
public class UserRegister extends Beans {
	String id=null;
	String passwd=null;
	String nickname=null;
	int age;
	int gender;
	public UserRegister(String id, String passwd, String nickname, int age, int gender) {
		super();
		this.id = id;
		this.passwd = passwd;
		this.nickname = nickname;
		this.age = age;
		this.gender = gender;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	
}
