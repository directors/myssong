package com.ssong.bean;

import java.beans.Beans;

public class UserFavoriteUser extends Beans {
	int favoriteuidx;
	String userNickname;
	public String getUserNickname() {
		return userNickname;
	}
	public void setUserNickname(String userNickname) {
		this.userNickname = userNickname;
	}
	float favoritescore;
	public UserFavoriteUser() {
		super();
	}
	public UserFavoriteUser(int favoriteuidx, float favoritescore) {
		super();
		this.favoriteuidx = favoriteuidx;
		this.favoritescore = favoritescore;
	}
	public int getFavoriteuidx() {
		return favoriteuidx;
	}
	public void setFavoriteuidx(int favoriteuidx) {
		this.favoriteuidx = favoriteuidx;
	}
	public float getFavoritescore() {
		return favoritescore;
	}
	public void setFavoritescore(float favoritescore) {
		this.favoritescore = favoritescore;
	}
}
