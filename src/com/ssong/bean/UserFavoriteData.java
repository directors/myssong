package com.ssong.bean;

import java.beans.Beans;

public class UserFavoriteData extends Beans {
	int uidx;
	int midx;
	float rating;
	public UserFavoriteData() {
		super();
	}
	public UserFavoriteData(int uidx, int midx, float rating) {
		super();
		this.uidx = uidx;
		this.midx = midx;
		this.rating = rating;
	}
	public int getUidx() {
		return uidx;
	}
	public void setUidx(int uidx) {
		this.uidx = uidx;
	}
	public int getMidx() {
		return midx;
	}
	public void setMidx(int midx) {
		this.midx = midx;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
}
