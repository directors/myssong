package com.ssong.bean;
/**
 * 상세한 음악
 * @author changwoncheo
 *
 */
public class DetailMusic extends SimpleMusic {
	String date;
	String genre;
	String albumname;
	public String getDate() {
		return date;
	}
	@Override
	public String toString() {
		return super.toString() + ", DetailMusic [date=" + date + ", genre=" + genre
				+ ", albumname=" + albumname + "]";
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getAlbumname() {
		return albumname;
	}
	public void setAlbumname(String albumname) {
		this.albumname = albumname;
	}
	/**
	 * 상세 페이지에 보일 때 사용하는 빈즈 객체
	 * @param midx
	 * @param artist
	 * @param title
	 * @param imagePath
	 * @param rating
	 * @param date
	 * @param genre
	 * @param albumname
	 */
	public DetailMusic(int midx, String artist, String title, String imagePath, float rating,
			 String date, String genre, String albumname){
		super(midx, artist, title, imagePath, rating);
		this.date = date;
		this.genre = genre;
		this.albumname = albumname;
	}
}
