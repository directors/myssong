package com.ssong.bean;

import java.beans.Beans;
import java.util.ArrayList;

public class Playlist extends Beans {

	int listidx;
	String title;
	int songcount;
	ArrayList<ListMusic> songs;
	
	public ArrayList<ListMusic> getSongs() {
		return songs;
	}
	public void setSongs(ArrayList<ListMusic> songs) {
		setSongcount(songs.size());
		this.songs = songs;
	}
	
	public int getListidx() {
		return listidx;
	}
	public void setListidx(int listidx) {
		this.listidx = listidx;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getSongcount() {
		return songcount;
	}
	public void setSongcount(int songcount) {
		this.songcount = songcount;
	}

}
