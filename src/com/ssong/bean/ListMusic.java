package com.ssong.bean;

import java.beans.Beans;

/**
 * playlist에 들어가는 음악 정보
 */
public class ListMusic extends SimpleMusic {

	int iidx;
	String genre;


	/**
	 * 상세페이지가 아닌 간략한 목록형태로 주어질때 필요한 객체
	 * 
	 * @param iidx
	 *            - item index
	 */

	public ListMusic(int iidx, int midx, String artist, String title, String genre){
		super(midx, artist, title, "", 0);
		this.iidx = iidx;
		this.genre = genre;
	}

	public int getIidx() {
		return iidx;
	}
	public void setIidx(int iidx) {
		this.iidx = iidx;
	}

	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	@Override
	public String toString() {
		return "SimpleMusic [imagePath=" + imagePath + ", title=" + title
				+ ", artist=" + artist + ", rating=" + rating + ", midx="
				+ midx + "]";
	}
}
