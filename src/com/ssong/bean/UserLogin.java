package com.ssong.bean;

import java.beans.Beans;

/**
 * 사용 로그인 사용되는 클래스
 * @author changwoncheo
 *
 */
public class UserLogin extends Beans {
	int uidx;
	String uid;
	String nickname;
	int ugender;
	int survey;
	public UserLogin(int uidx, String uid, String nickname, int ugender,
			int survey, int uage) {
		super();
		this.uidx = uidx;
		this.uid = uid;
		this.nickname = nickname;
		this.ugender = ugender;
		this.survey = survey;
		this.uage = uage;
	}
	public int getSurvey() {
		return survey;
	}
	public int isSurvey() {
		return survey;
	}
	public void setSurvey(int survey) {
		this.survey = survey;
	}
	public int getUidx() {
		return uidx;
	}
	public void setUidx(int uidx) {
		this.uidx = uidx;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public int getUgender() {
		return ugender;
	}
	public void setUgender(int ugender) {
		this.ugender = ugender;
	}
	public int getUage() {
		return uage;
	}
	public void setUage(int uage) {
		this.uage = uage;
	}
	int uage;
	
	
}
