package com.ssong.bean;

import java.beans.Beans;
/**
 * 가수 TOP5 통계를 위 사용하는 클래
 * @author changwoncheo
 *
 */
public class SingerStatistic extends Beans{
//	int midx;
	String artistname;
	public int getPreferCount() {
		return preferCount;
	}
	public void setPreferCount(int preferCount) {
		this.preferCount = preferCount;
	}

	int preferCount;
	public SingerStatistic(String artistname,int preferCount){
		this.artistname = artistname;
		this.preferCount = preferCount;
	}
	public String getArtistname() {
		return artistname;
	}

	public void setArtistname(String artistname) {
		this.artistname = artistname;
	}
}
