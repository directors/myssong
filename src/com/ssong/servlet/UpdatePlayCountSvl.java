package com.ssong.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.db.Database;
import com.ssong.json.JsonMaker;

/**
 * Servlet implementation class PlayCountUpdateSvl
 */
@WebServlet("/PlayCountUpdateSvl")
public class UpdatePlayCountSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdatePlayCountSvl() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		HttpSession session = request.getSession();
//		System.out.println(session.getLastAccessedTime());
//		long time1 = System.currentTimeMillis ();
//		System.out.println(time1);
//		System.out.println(time1 - session.getLastAccessedTime());
//	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer uidx = (Integer) session.getAttribute("uindex");
		//플레이카운트 매크로를 막자
		String midxParam = request.getParameter("midx");
		Long lastAccessTime = (Long)session.getAttribute("lastupdatetime");
		if(lastAccessTime==null)
			lastAccessTime = 0L;
		long ctime = System.currentTimeMillis ();
		long timegap = ctime - lastAccessTime;
		System.out.println("gap : "+(timegap));
		
		session.setAttribute("lastupdatetime", ctime);	//갱신
		Database db = null;
		String result = null;

		//요청 준비
		ServletOutputStream os = response.getOutputStream();
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-cache");
		try{								//1분안에 요청시 기각	
			if(uidx!=null && timegap > (2000* 60) && midxParam!= null){
				int midxInt = Integer.parseInt(midxParam);
				db = new Database();
				//플레이 카운트 업데이트
				if(db.updatePlayCount(uidx, midxInt)){
					System.out.println("playCount update:"+uidx+":"+midxInt);
					result = JsonMaker.getCode(true);
				}
			}else{
				result = JsonMaker.getCode(false);
				System.out.println("can't update playCount");
			}
		}catch(Exception e){
			e.printStackTrace();
			result = JsonMaker.getCode(false);
		}finally{
			if(db!=null){
				db.Close();
			}
			os.write(result.getBytes());
		}
	}

}
