package com.ssong.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.db.Database;
import com.ssong.bean.Poem;

/**
 * Servlet implementation class PoemSvl
 */
@WebServlet("/PoemSvl")
public class PoemSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PoemSvl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//인증검사
		HttpSession session = request.getSession();
		Integer uidx = (Integer)session.getAttribute("uindex");
		Integer usurvey = (Integer)session.getAttribute("usurvey");

//		uidx = 8003;
		Database db=null;

		//
		try{
			if(uidx!=null && usurvey == 1){
				//최신 파일이름 가져올 것
				db = new Database();
				
				Poem poem = db.getPoem();
				request.setAttribute("POEM", poem);

				RequestDispatcher dispatcher = request
						.getRequestDispatcher("base.jsp?BODYNAME=poem.jsp");
				dispatcher.forward(request, response);
				//페이지 생성

			}else{
				response.sendRedirect("");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(db!=null){
				db.Close();
			}
		}
		
		//Database 방문
		
		//페이지 전송
	}

}
