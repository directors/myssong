package com.ssong.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.bean.SimpleMusic;
import com.ssong.db.Database;
import com.ssong.json.JsonMaker;

/**
 * Servlet implementation class FirstRatingSvl
 */
@WebServlet("/FirstRatingSvl")
public class FirstRatingSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FirstRatingSvl() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 세션체크
		HttpSession session = request.getSession();
		Integer uindex = (Integer) session.getAttribute("uindex");
		Database db = null;
		String count = "";
		try {
			// test
			//uindex = 9005;
			//
			if (uindex != null) {
				db = new Database();
				// 접근제어
				// if(!israting){
				// 랜덤 30개 가져옴
				List<SimpleMusic> musicList = db.getRandomMusic(uindex);

				request.setAttribute("MUSICLIST", musicList);
				// rattingcount
				long ratingcount = db.getCountRating(uindex);
				System.out.println("ratingcount : "+ratingcount);
				/*if(ratingcount>=30){
					request.setAttribute("RATINGOK", true);

				}*/
				request.setAttribute("RATINGCOUNT", ratingcount);
				
				/**
				 * 평가한 항목이 30개가 넘을 경우
				 * 그리고 finish 를 눌렀을때
				 */
				if(ratingcount>=30 && session.getAttribute("surveyfinish")!=null){
					session.setAttribute("usurvey", 1);
					session.removeAttribute("surveyfinish");
					//db업데이트
					db.setSurveyValue(uindex, 1);
					response.sendRedirect("Poem.do");
				}else{
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("first_rating_songs.jsp");
				dispatcher.forward(request, response);
				}
				// 페이지 생성 후 전달
				// }
				/*
				 * else{ //로그인 페이지로 }
				 */
			} else {
				// 로그인 페이지로
				response.sendRedirect("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.Close();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Integer uindex = (Integer) session.getAttribute("uindex");
		Database db = null;
		String count = "";
		try {
			// test
			//uindex = 8003;
			//
			if (uindex != null) {
				db = new Database();
				// 응답전송 준비
				ServletOutputStream os = response.getOutputStream();
				response.setCharacterEncoding("UTF-8");
				response.setContentType("application/json");
				response.setHeader("Cache-Control", "no-cache");
				
				// 접근제어
				/**
				 * 설문조사를 끝냈을 경우
				 */
				String finish = request.getParameter("finish");
				if(finish!=null){
					session.setAttribute("surveyfinish", finish);
				}
				
				ArrayList<SimpleMusic> songrating = JsonMaker
						.parseArrayJson(request.getParameter("ratinglist"));
				if(validateRating(songrating)==false){
					System.out.println("Invalidate rating");
					os.write(JsonMaker.getCode(false).getBytes());
				}else{
					System.out.println("validate rating");
				// 디비에 저장 레이팅값
				boolean setRating = db.setRatings(songrating, uindex);

				if(setRating)
					os.write(JsonMaker.getCode(true).getBytes());
				else
					os.write(JsonMaker.getCode(false).getBytes());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.Close();
			}
		}
	}
	protected boolean validateRating(ArrayList<SimpleMusic> songrating){
		if(songrating!= null){
			for(SimpleMusic music:songrating){
				float rating = music.getRating();
				if(music.getRating()==0.5f
						||music.getRating()==1.0f
						||music.getRating()==1.5f
						||music.getRating()==2.0f
						||music.getRating()==2.5f
						||music.getRating()==3.0f
						||music.getRating()==3.5f
						||music.getRating()==4.0f
						||music.getRating()==4.5f
						||music.getRating()==5.0f
						){
				}else{
					return false;
				}
			}
			return true;
		}else{
			return false;
		}
	}
}
