package com.ssong.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.bean.UserLogin;
import com.ssong.bean.UserRegister;
import com.ssong.db.Database;

/**
 * Servlet implementation class RegisterSvl
 */
@WebServlet("/RegisterSvl")
public class RegisterSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterSvl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Database db = null;
		request.setCharacterEncoding("utf-8");
		String id = request.getParameter("userid");
		String nickname = request.getParameter("nickname");
		String password = request.getParameter("password");
		String password_r = request.getParameter("password-r");
		String gender =request.getParameter("gender");
		String age= request.getParameter("age");
		boolean result = false;
		//session
		HttpSession session = null;
		session = request.getSession();
		
//		byte[] resultCode= null;
		//JSON준비
//		response.setCharacterEncoding("UTF-8");
//		response.setContentType("application/json");
//		response.setHeader("Cache-Control", "no-cache");
		System.out.println("id:"+id+",nick:"+nickname+",passwd:"+password+",passwordr:"+password_r+",gender:"+gender+",age"+age);
		
		if(checkValidate(id, nickname, password, password_r, gender, age)){
			try{
				int genderInt = Integer.parseInt(gender); 
				int ageInt = Integer.parseInt(age);
				db = new Database();
				UserRegister userdata= new UserRegister(id, password, nickname, ageInt, genderInt);
				if(db.registerUser(userdata)){
					result = true;
//					resultCode = JsonMaker.getCode(true).getBytes();
					
					//기본리스트 추가
					UserLogin user = db.getUser(id);
					db.addPlaylist(user.getUidx(), "기본리스트");
					
				}else{
					result = false;
//					resultCode = JsonMaker.getCode(false).getBytes();
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(db!=null){
					db.Close();
				}
			}
		}else{
			result = false;
//			resultCode = JsonMaker.getCode(false).getBytes();
		}
		//결과 전송
//		if(resultCode== null){
//			resultCode = JsonMaker.getCode(false).getBytes();
//		}
//		ServletOutputStream os = response.getOutputStream();
//		os.write(resultCode);
//		os.close();
		if(result==true){
			session.setAttribute("register", true);
			response.sendRedirect("");
		}else{
			response.getOutputStream().write("<script>history.back();</script>".getBytes());
		}
	}
	boolean checkValidate(String id,
			String nickname,
			String password,
			String password_r,
			String gender,
			String age){
		if(id == null
				|| password == null
				|| password_r ==null
				|| nickname ==null
				|| gender ==null
				|| age ==null
				|| password.length()==0
				|| password_r.length()==0
				|| id.length() ==0
				|| nickname.length() ==0
				|| gender.length() ==0
				|| age.length() ==0
				|| !password.equals(password_r)
				|| !checkEmpty(id)
				|| !checkEmpty(password)
				|| !checkEmpty(password_r)
				|| !checkEmpty(nickname)
				|| !checkEmpty(gender)
				|| !checkEmpty(age)
				){
			System.out.println("check false");
			return false;
		}
		if(lengthValidation(id, nickname, password, password_r, gender, age)==false){
			System.out.println(nickname.length());
			System.out.println("check false");
			return false;
		}
		if(!checkIdValidation(id)){
			return false;
		}
		return true;
	}
	boolean lengthValidation(String id,
			String nickname,
			String password,
			String password_r,
			String gender,
			String age){
		try{
			int idlength = id.length();
			int passlength = password.length();
			int nicklength = nickname.length();
			int ageInt = Integer.parseInt(age);
			if(idlength<4 || idlength>16){
				return false;
			}
			if(passlength<4 || passlength>16){
				return false;
			}
			if(nicklength<2 || nicklength>8){
				return false;
			}
			if(!gender.equals("1") && !gender.equals("0")){
				return false;
			}

			if(ageInt==10 || ageInt==20 || ageInt==30 ||ageInt==40 ||ageInt==50){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;

	}
	boolean checkEmpty(String str){
		if(str.indexOf(" ")!=-1){
			return false;
		}
		return true;
	}
	/**
	 * 소문자, 숫자, 대문자만 아이디
	 * @param id
	 * @return
	 */
	boolean checkIdValidation(String id){
		if(id == null)
			return false;
		else{
			byte[] idchar = id.getBytes();
			for (int i =0 ; i<id.length() ;i++){
				if( ('a' <= idchar[i] && idchar[i] <= 'z') || (idchar[i] >= '0' && idchar[i] <='9')
						|| (idchar[i] >='A' && idchar[i] <='Z') ){
					
				}else{
					return false;
				}
			}
		}
		return true;
	}

}
