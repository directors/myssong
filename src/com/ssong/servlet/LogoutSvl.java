package com.ssong.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutSvl extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		Integer uindex = null;
		HttpSession session = req.getSession();
		uindex = (Integer)session.getAttribute("uindex");
		try{
			if(uindex!=null){
				session.removeAttribute("uindex");
				session.removeAttribute("uid");
				session.removeAttribute("unick");
				session.removeAttribute("ugender");
				session.removeAttribute("usurvey");
			}
		}catch(Exception e){
		}finally{//항상 로그인 페이지로 이동
			resp.sendRedirect("Redirect.do");
		}
	}
}
