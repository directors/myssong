package com.ssong.servlet;

import javax.servlet.http.*;
import javax.servlet.*;

import java.io.*;

import com.mysql.jdbc.Constants;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.oreilly.servlet.MultipartRequest;
import com.ssong.db.ResourceConstants;

import com.ssong.db.Database;

public class UploadCSVSvl extends HttpServlet {
      public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    	  	Database db = new Database();
    	  	
			//String savepath = request.getRealPath("/csv");
			String savepath = ResourceConstants.Favorite;
			int maxSize = 1024*1024*100;
			String encType = "euc-kr";
			
			String lastcsv = db.getCurrentCSV(); //예전
			
			MultipartRequest req = new MultipartRequest(request, savepath, maxSize, encType, new DefaultFileRenamePolicy());
			
			String filepath = req.getFilesystemName("filepath");
			
			String curcsv = savepath + File.separatorChar + filepath; //현재
			
			db.setCurrentCSV(filepath);
			
			File f = new File(savepath + File.separatorChar + lastcsv);
			f.delete();
			
			db.Close();
			
			System.out.println("uploaded csv at : " + curcsv);
      }
}
