package com.ssong.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.bean.Playlist;
import com.ssong.bean.SimpleMusic;
import com.ssong.db.Database;

/**
 * Servlet implementation class DetailSearchSvl
 */
@WebServlet("/DetailSearchSvl")
public class DetailSearchSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailSearchSvl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.setCharacterEncoding("utf-8");
    	HttpSession session = request.getSession();
    	Integer uindex = (Integer) session.getAttribute("uindex");
    	String query =null;
    	try{
    		query =  com.ssong.util.Util.GetEncodeByUtf8(request.getParameter("query"));
    	}catch(Exception e){
    		query = null;
    	}
		//System.out.println("query : "+query);
    	String type = request.getParameter("type");
    	String page = request.getParameter("page");
    	//uindex = 8004;
    	Database db = null;
		ArrayList<Playlist> playlist= null;
    	try{
        	//1)인증
    		if(uindex != null && query !=null && type!=null && page!=null){
    			System.out.println("query : "+query);
    	    	//2)검색
       			ArrayList<SimpleMusic> searchResultList = null;
       			int itemCount = 0 ;
    			db = new Database();
    			int pageInt = Integer.parseInt(page);
    	    	playlist = db.getPlaylistNotContainMusic(uindex);
    	    	
    			if(type.equals("ARTIST")){
    				itemCount = db.getCountArtistearch(query);
    				if(pageInt<1 || pageInt>((itemCount / 24)+1)){
    					//틀린 유효성
    				}
    				//페이지 유효성 검사
    				searchResultList = db.searchArtist(query,(pageInt-1)*24,24);
    				request.setAttribute("TYPE", "ARTIST");
    			}else if(type.equals("MUSIC")){
    				itemCount = db.getCountMusicSearch(query);
    				if(pageInt<1 || pageInt>((itemCount / 24)+1)){
    					//틀린 유효성
    				}
    				//페이지 유효성 검사
    				searchResultList = db.searchMusic(query,(pageInt-1)*24,24);
    				request.setAttribute("TYPE", "MUSIC");
    			}else if(type.equals("ARTISTMUSIC")){
    				itemCount = db.getCountArtistMusicSearch(query);
    				if(pageInt<1 || pageInt>((itemCount / 24)+1)){
    					//틀린 유효성
    				}
    				//페이지 유효성 검사
    				searchResultList = db.searchArtistMusic(query, (pageInt-1)*24, 24);
    				request.setAttribute("TYPE", "ARTISTMUSIC");
    				//가수 이름으로 곡 검색
    			}else{
    				
    				//검색 실패
    			}
    	    	//3)검색페이지
    			System.out.println("totalcount : "+itemCount);	//아이템 개수
    			request.setAttribute("RESULTMUSICLIST", searchResultList);
    			if((((pageInt-1)/5)*5) == 0){
        			request.setAttribute("BEFORE", false);
//        			System.out.println("BEFORE : " + false);
    			}else{
        			request.setAttribute("BEFORE", true);
//        			System.out.println("BEFORE : " + true);
    			}
				//나눠 떨어질때는 이전 값으로 따라감
    			if(pageInt%5 != 0 ){
    				request.setAttribute("PAGEBEGIN", ((pageInt/5)*5)+1);
//        			System.out.println("PAGEBEGIN : " + ((pageInt/5)*5)+1);
    			}else{
        			request.setAttribute("PAGEBEGIN", (((pageInt/5)-1)*5)+1);
//        			System.out.println("PAGEBEGIN : " + (((pageInt/5)-1)*5)+1);
    			}
    			
    			if((((pageInt-1)/5)+1)*5 < ((itemCount / 24)+1)){
    				//나눠 떨어질때는 이전 값으로 따라감
        			if(pageInt%5 != 0 ){
        				request.setAttribute("PAGEEND", ((pageInt/5)+1)*5);
//            			System.out.println("PAGEEND : " + ((pageInt/5)+1)*5);
        			}else{
            			request.setAttribute("PAGEEND", ((pageInt/5))*5);
//            			System.out.println("PAGEEND : " + ((pageInt/5))*5);
        			}
        			request.setAttribute("NEXT", true);
//        			System.out.println("NEXT : " + true);
    			}else{
        			request.setAttribute("PAGEEND", ((itemCount / 24)+1));
//        			System.out.println("PAGEEND : " + ((itemCount / 24)+1));
        			request.setAttribute("NEXT", false);
//        			System.out.println("NEXT : " +false);
    			}
    			request.setAttribute("PLAYLIST", playlist);
    			request.setAttribute("PAGEOFFSET", pageInt);
    			request.setAttribute("QUERY", query);
//    			System.out.println("PAGEOFFSET : " + pageInt);
//    			System.out.println("QUERY : " + query);
    			RequestDispatcher dispatcher = request.getRequestDispatcher("base.jsp?BODYNAME=detail_search.jsp");
    			dispatcher.forward(request, response);
    		}else if(uindex == null){
    	    	//4)로그인 페이지
				response.sendRedirect("");
    		}else if(query == null || type==null || page== null){
				response.sendRedirect("");
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally{
    		if(db!=null){
    			db.Close();
    		}
    	}
	}
//
//	/**
//	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//	}

}
