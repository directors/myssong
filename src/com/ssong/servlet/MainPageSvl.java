package com.ssong.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.bean.DetailMusic;
import com.ssong.bean.Playlist;
import com.ssong.bean.SimpleMusic;
import com.ssong.bean.UserFavoriteData;
import com.ssong.db.Database;
import com.ssong.db.ResourceConstants;
import com.ssong.file.FavoriteSpliter;
import com.ssong.bean.Poem;

/**
 * Servlet implementation class MainPageSvl
 */
@WebServlet("/MainPageSvl")
public class MainPageSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainPageSvl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//인증검사
		HttpSession session = request.getSession();
		Integer uidx = (Integer)session.getAttribute("uindex");
		Integer usurvey = (Integer)session.getAttribute("usurvey");
		String filename = null;
		List<UserFavoriteData> userFavoriteList = null;
		ArrayList<Playlist> playlist= null;
//		uidx = 8003;
		Database db=null;

		ArrayList<SimpleMusic> resultMusicList = null;
		List<SimpleMusic> simpleList = null;
		//
		try{
			if(uidx!=null && usurvey == 1){
				//최신 파일이름 가져올 것
				db = new Database();
				filename = db.getCurrentCSV();
				File csvFile = new File(ResourceConstants.Favorite+"/"+filename);
				if(!csvFile.exists()){
					filename = "recommend3.csv";
					System.out.println("csv file not Found");
				}else{
					System.out.println("csv file Found : "+filename);
				}
				
				//추천 데이터 가져옴 (File IO)
				userFavoriteList = FavoriteSpliter.getUserFavoriteList(filename,uidx,0,1000);
				long seed = System.nanoTime();
				Collections.shuffle(userFavoriteList, new Random(seed));
				if(userFavoriteList.size() < 30){	//최대크기를 가져옴
					userFavoriteList = userFavoriteList.subList(0, userFavoriteList.size());
				}else{	//딱 29개를 가져옴
					userFavoriteList = userFavoriteList.subList(0, 29);
				}
				
				//음악 및 앨범 데이터를 가져옴
				playlist = db.getPlaylistNotContainMusic(uidx);
				if(userFavoriteList != null && userFavoriteList.size()!=0){
					resultMusicList = db.getMainPageMusicItems(userFavoriteList);
				//System.out.println((DetailMusic)resultMusicList.get(0));
					simpleList= resultMusicList.subList(1, resultMusicList.size());
					for (SimpleMusic music : simpleList){
						break;
						//System.out.println(music);
					}
					request.setAttribute("EXISTS", true);
				}else{
					resultMusicList=new ArrayList<SimpleMusic>();
					simpleList = new ArrayList<SimpleMusic>();
					Poem poem = db.getPoem();
					request.setAttribute("POEM", poem);
					request.setAttribute("EXISTS", false);
				}
				request.setAttribute("PLAYLIST", playlist);
				
				try{
					request.setAttribute("FIRSTMUSIC", (DetailMusic)resultMusicList.get(0));
				}catch(Exception e){
					request.setAttribute("FIRSTMUSIC", null);
				}
				//else
				//	request.setAttribute("FIRSTMUSIC", new DetailMusic(0,null,null,null,0,null,null,null));
				request.setAttribute("RESULTMUSICLIST", simpleList);
				
				String nickname = null;
				nickname = (String)session.getAttribute("unick");
				if(nickname == null){
					nickname="";
				}
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("base.jsp?BODYNAME=main.jsp");
				dispatcher.forward(request, response);
				//페이지 생성

			}else{
				response.sendRedirect("");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(db!=null){
				db.Close();
			}
		}
		
		//Database 방문
		
		//페이지 전송
	}

	protected void testReadCSV(HttpServletRequest request,HttpServletResponse response){
		PrintStream s=null;
		try {
			s = new PrintStream(response.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ArrayList<UserFavoriteData> arr = FavoriteSpliter.getUserFavoriteList("recommend.csv",8003,0,10);
		//s.println("recommend.csv");
		//s.println("size : "+arr.size());
		for (UserFavoriteData d:arr){
			s.println("uidx : "+d.getUidx()+", umidx : "+d.getMidx()+", rating : "+d.getRating());
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
