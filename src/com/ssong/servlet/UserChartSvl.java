package com.ssong.servlet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.bean.DateYearRanking;
import com.ssong.bean.GenreStatistic;
import com.ssong.bean.SimpleMusic;
import com.ssong.bean.SingerStatistic;
import com.ssong.bean.UserFavoriteUser;
import com.ssong.bean.UserLogin;
import com.ssong.db.Database;
import com.ssong.db.ResourceConstants;
import com.ssong.file.FavoriteSpliter;

/**
 * Servlet implementation class UserCharSvl
 */
@WebServlet("/UserChart.do")
public class UserChartSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserChartSvl() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession();
		Integer uindex;

		try {
			uindex = Integer.parseInt(request.getParameter("index"));

		} catch (Exception e) {
			uindex = (Integer) session.getAttribute("uindex");
		}
		// Integer uindex = new Integer(request.getParameter("uindex"));
		Database db = null;
		try {
			// 1)인증
			if (uindex != null) {
				// 2) 통계데이타
				db = new Database();

				if (db.getUindexExist(uindex) == false) {
					response.sendRedirect("");
					return;
				}

				String filename = db.getCurrentKN1();
				if (filename == null) {
					response.sendRedirect("");
					return;
				}
				UserFavoriteUser favoriteUser = null;
				File csvFile = new File(ResourceConstants.Favorite + "/"
						+ filename);
				
				if (!csvFile.exists()) {
					filename = "knn_1409235294.csv";
					favoriteUser = FavoriteSpliter.getUserFavoriteUser(
							filename, uindex);
					if (favoriteUser != null)
						System.out.println("favorite data "
								+ favoriteUser.getFavoriteuidx() + ":"
								+ favoriteUser.getFavoritescore());
					else {
						System.out.println("csv에 존재하지 않음");
					}
					System.out.println("csv file not Found");
				} else {
					System.out.println("csv file Found : " + filename);

					// 유사 사용자 데이터 가져옴 (File IO)
					favoriteUser = FavoriteSpliter.getUserFavoriteUser(
							filename, uindex);
					if (favoriteUser != null)
						System.out.println("favorite data "
								+ favoriteUser.getFavoriteuidx() + ":"
								+ favoriteUser.getFavoritescore());
					else {
						System.out.println("csv에 존재하지 않음");
					}
				}

				// 선호 장르
				ArrayList<GenreStatistic> genre_arr = db
						.getGenrePercent(uindex);
				// 선호 가수
				ArrayList<SingerStatistic> artist_rank_arr = db
						.getArtistRank(uindex);
				// 총 평점내린 갯수
				long ratingcount = db.getCountRating(uindex);
				// 총 들은 노래 갯수
				long hearsongcount = db.getHearSongCount(uindex);
				// 리스트에 담은 노래 갯수
				long listsongscount = db.getListSongCount(uindex);
				// 선호 하는 년대
				ArrayList<DateYearRanking> dateranklist = db
						.getDatePercent(uindex);
				// 선호 음악들
				ArrayList<SimpleMusic> songslist = db.getSongsRank(uindex);
				// 유사사용자
				if(favoriteUser != null){
				favoriteUser.setUserNickname(db.getUserNickname(favoriteUser.getFavoriteuidx()));
				favoriteUser.setFavoritescore((int)(favoriteUser.getFavoritescore()*100));
				}
				// for (int i = 0; i < songslist.size(); i++)
				// System.out.println(songslist.size() +
				// songslist.get(i).toString());
				String nickname = null;
				nickname = (String) db.getUserNickname(uindex);
				if (nickname == null) {
					nickname="";
				}
				request.setAttribute("NICKNAME", nickname);
				request.setAttribute("GENREARRAY", genre_arr);
				request.setAttribute("ARTISTRANK", artist_rank_arr);
				request.setAttribute("RATINGCOUNT", ratingcount);
				request.setAttribute("SONGCOUNT", hearsongcount);
				request.setAttribute("LISTCOUNT", listsongscount);
				request.setAttribute("DATERANKLIST", dateranklist);
				request.setAttribute("SONGSRANK", songslist);
				request.setAttribute("FAVORITEUSER", favoriteUser);

				RequestDispatcher dispatcher = request
						.getRequestDispatcher("base.jsp?BODYNAME=stat_charts.jsp");
				dispatcher.forward(request, response);
			} else if (uindex == null) {
				// 4)로그인 페이지
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.Close();
			}
		}
	}

}
