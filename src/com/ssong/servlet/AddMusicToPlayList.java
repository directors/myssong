package com.ssong.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.db.Database;
import com.ssong.json.JsonMaker;

/**
 * Servlet implementation class AddMusicToPlayList
 */
@WebServlet("/AddMusicToPlayList")
public class AddMusicToPlayList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddMusicToPlayList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String resultData = null;
		HttpSession session = request.getSession();
		Database db = null;
		Integer uidx = (Integer) session.getAttribute("uindex");
		//uidx = 8003;
		String albumidx = request.getParameter("albumidx");
		String musiclist = request.getParameter("musiclist");
		System.out.println(uidx+":"+albumidx+":"+musiclist);
		if(uidx!=null && albumidx!= null && musiclist != null){
			try{
				db = new Database();
				int albumidx_val = Integer.parseInt(albumidx);
				ArrayList<Long> arr = JsonMaker.parseMusicIndexArray(musiclist);
				int currentMusicCount =db.getPlaylistMusicCount(albumidx_val);//현재 음악의 개수
				int possibleCount = Database.PLAYLIST_MUSIC_COUNT - currentMusicCount;
				if(arr.size()>possibleCount){
					System.out.println("over count when musics are inserted");
					int deleteCount = arr.size()-possibleCount ; 
					for (int i=0 ; i<deleteCount ;i++){
						try{
							arr.remove(arr.size()-1);
						}catch(Exception e){
							System.out.println("album delete bug occur");
						}
					}
				}
				if(db.insertMusicToPlayList(uidx,albumidx_val,arr)){
					resultData = JsonMaker.getCode(true);
					System.out.println("[AddMusicToPlayList]플레이리스트 등록 성공");
				}else{
					resultData = JsonMaker.getCode(false);
					System.out.println("[AddMusicToPlayList]플레이리스트 등록 실패");
				}
				
			}catch(Exception e){
				e.printStackTrace();
				resultData = JsonMaker.getCode(false);
				System.out.println("[AddMusicToPlayList]플레이리스트 등록 실패");
			}finally{
				if(db!=null){
					db.Close();
				}
			}
		}
		ServletOutputStream os = response.getOutputStream();
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-cache");
		if(resultData== null){
			resultData = JsonMaker.getCode(false);
		}
		os.write(resultData.getBytes());
	}

}
