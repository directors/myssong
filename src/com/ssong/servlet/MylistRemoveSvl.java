package com.ssong.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.db.Database;

/**
 * Servlet implementation class MylistAddSvl
 */
@WebServlet("/MylistRemove")
public class MylistRemoveSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MylistRemoveSvl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Database db = new Database();
		HttpSession session = request.getSession();
		Integer uidx = (Integer) session.getAttribute("uindex");
		
		try {
			//test
//			uidx=8003;
			if(uidx!=null){
			int pidx = Integer.parseInt(request.getParameter("remove-idx"));
			db.removePlaylist(uidx, pidx); //세션으로
			}else{
				response.sendRedirect("");
			}
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			db.Close();
		}

		response.sendRedirect("Mylist.do");
	}

}
