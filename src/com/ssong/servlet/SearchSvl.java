package com.ssong.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.Util;
import com.ssong.bean.Playlist;
import com.ssong.bean.SimpleMusic;
import com.ssong.db.Database;

/**
 * Servlet implementation class SearchSvl
 */
@WebServlet("/SearchSvl")
public class SearchSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchSvl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * 음악/가수 검색을 처리하는 페이지
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.setCharacterEncoding("utf-8");
    	HttpSession session = request.getSession();
    	Integer uindex = (Integer) session.getAttribute("uindex");
    	String query = null;
    	try{
    		query =  com.ssong.util.Util.GetEncodeByUtf8(request.getParameter("query"));
    	}catch(Exception e){
    		query = null;
    	}
    	System.out.println("query: "+query);
//    	uindex = 8003;
    	Database db = null;
		ArrayList<Playlist> playlist= null;
    	try{
        	//1)인증
    		if(uindex != null && query !=null){
    	    	//2)검색
       			ArrayList<SimpleMusic> musicList = null;
       			ArrayList<SimpleMusic> artistList = null;
    			db = new Database();
    			musicList = db.searchMusic(query,0,12);
    			artistList = db.searchArtist(query,0,12);
    	    	playlist = db.getPlaylistNotContainMusic(uindex);
    			//System.out.println(musicList.size());
    			//System.out.println(artistList.size());
    	    	//3)검색페이지
    	    	
    			request.setAttribute("PLAYLIST", playlist);
    			request.setAttribute("QUERY", query);
    			request.setAttribute("MUSICLIST", musicList);
    			request.setAttribute("ARTISTLIST", artistList);
    			RequestDispatcher dispatcher = request.getRequestDispatcher("base.jsp?BODYNAME=search.jsp");
    			dispatcher.forward(request, response);
    		}else if(uindex == null){
    	    	//4)로그인 페이지
    		}else if(query == null){
    			
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally{
    		if(db!=null){
    			db.Close();
    		}
    	}
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
