package com.ssong.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.db.Database;

/**
 * Servlet implementation class MylistAddSvl
 */
@WebServlet("/MylistAdd")
public class MylistAddSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MylistAddSvl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		Database db = new Database();
		HttpSession session = request.getSession();
		Integer uidx = (Integer) session.getAttribute("uindex");
		try {
//			uidx=8003;
			if(uidx!=null){
			//String playlist = request.getParameter("listname");
			String playlist = new String(request.getParameter("listname").getBytes("8859_1"),"UTF-8");
			
			if (db.getPlaylistCount(uidx) < 11) //세션으로
			{
				if (playlist.length() < 11) db.addPlaylist(uidx, playlist); //세션으로
			}
			}else{
				response.sendRedirect("");
			}
			
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			db.Close();
		}

		response.sendRedirect("Mylist.do");
	}

}
