package com.ssong.servlet;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.bean.UserFavoriteData;
import com.ssong.file.FavoriteSpliter;

/**
 * Servlet implementation class FileReaderTest
 */
@WebServlet("/FileReaderTest")
public class FileReaderTest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileReaderTest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		testReadCSV(request,response);
		
	}

	protected void testReadCSV(HttpServletRequest request,HttpServletResponse response){
		PrintStream s=null;
		try {
			s = new PrintStream(response.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ArrayList<UserFavoriteData> arr = FavoriteSpliter.getUserFavoriteList("recommend.csv",8003,0,10);
		s.println("recommend.csv");
		s.println("size : "+arr.size());
		for (UserFavoriteData d:arr){
			s.println("uidx : "+d.getUidx()+", umidx : "+d.getMidx()+", rating : "+d.getRating());
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
