package com.ssong.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.bean.UserLogin;
import com.ssong.db.Database;

public class LoginSvl extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Database db = null;
		HttpSession session = (HttpSession)request.getSession();
		try {
			String username = request.getParameter("username");
			String password = request.getParameter("passwordsha1");
			System.out.println(username+":"+password);
			if (username == null || username.equals("")||
					password == null || password.equals("")) {
				//1)로그인 페이지로
				response.sendRedirect("");
				System.out.println("로그인 실패");
			}else{
				db = new Database();
				//1)로그인 인증
				//2)로그인 성공 페이지로(첫 사용자 설문조사/ 메인페이지)
				UserLogin user = db.getUser(username, password);
				if(user!=null){
					session.setAttribute("uindex", user.getUidx());
					session.setAttribute("uid", user.getUid());
					session.setAttribute("unick", user.getNickname());
					session.setAttribute("ugender", user.getUgender());
					session.setAttribute("usurvey", user.getSurvey());
					System.out.println("로그인 성공");
					if(user.getSurvey()==0){
						response.sendRedirect("firstRating.do");
					}else{
						response.sendRedirect("mainPage.do");
					}
				}else{
					System.out.println("메인 페이지로 이동");
					response.sendRedirect("");
					//로그인 실패 페이지로
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("");
		} finally {
			if(db!=null){
				db.Close();
			}
		}
		
	}
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Integer uindex = 0;
		HttpSession session = req.getSession();
		Boolean registerBool = null;
		registerBool = (Boolean) session.getAttribute("register");
		uindex = (Integer)session.getAttribute("uindex");
		Integer survey = (Integer) session.getAttribute("usurvey");
		if(uindex== null || survey==null){
			//회원가입 성공시
			if(registerBool != null && registerBool==true){
				session.removeAttribute("register");
				req.setAttribute("register", true);
			}

			RequestDispatcher dispatcher = req
					.getRequestDispatcher("myssong_login.jsp");
			
			dispatcher.forward(req, resp);
		}else{
			if(survey==0){
				resp.sendRedirect("firstRating.do");
			}else{
				resp.sendRedirect("mainPage.do");
			}
		}
	}
}
