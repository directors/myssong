package com.ssong.servlet;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.bean.DetailMusic;
import com.ssong.bean.Playlist;
import com.ssong.bean.SimpleMusic;
import com.ssong.bean.UserFavoriteData;
import com.ssong.db.Database;
import com.ssong.file.FavoriteSpliter;

/**
 * Servlet implementation class RatedPageSvl
 */
@WebServlet("/RatedPage.do")
public class RatedPageSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RatedPageSvl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Database db = new Database();
		
		//인증검사
		HttpSession session = request.getSession();
		Integer uidx = (Integer)session.getAttribute("uindex");

//		uidx = 8003;

		ArrayList<Playlist> playlist= null;
		ArrayList<SimpleMusic> resultMusicList = null;
		//
		try{
			if (uidx != null){ 

				resultMusicList = db.getRatedMusicItems(uidx);

				request.setAttribute("RESULTMUSICLIST", resultMusicList);
				//음악 및 앨범 데이터를 가져옴
				playlist = db.getPlaylistNotContainMusic(uidx);

				request.setAttribute("PLAYLIST", playlist);
				RequestDispatcher dispatcher = request.getRequestDispatcher("base.jsp?BODYNAME=rated_list.jsp");
				dispatcher.forward(request, response);
			}else{
				response.sendRedirect("");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		db.Close();
	}
}
