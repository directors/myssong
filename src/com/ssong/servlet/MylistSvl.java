package com.ssong.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;

import com.ssong.db.Database;
import com.ssong.bean.Playlist;

/**
 * Servlet implementation class MylistSvl
 */
@WebServlet("/Mylist")
public class MylistSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MylistSvl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Database db = new Database();
		
		try {
			ArrayList<Playlist> playlist;
			HttpSession session = request.getSession();
	    	Integer uindex = (Integer) session.getAttribute("uindex");
//	    	uindex = (Integer)8003;
	    	if(uindex!=null){
	    		playlist = db.getPlaylist(uindex);

	    		request.setAttribute("PLAYLIST", playlist);

	    		RequestDispatcher dispatcher = request.getRequestDispatcher("base.jsp?BODYNAME=mylist.jsp");
	    		dispatcher.forward(request, response);
	    	}else{
	    		//로그인 페이지
	    	}
			
		} catch (Exception e){
			e.printStackTrace();
    		response.sendRedirect("");
		} finally {
			db.Close();
		}
	}

}
