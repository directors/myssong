package com.ssong.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssong.db.Database;

/**
 * Servlet implementation class MylistAddSvl
 */
@WebServlet("/MylistEdit")
public class MylistEditSvl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MylistEditSvl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Database db = new Database();
		HttpSession session = request.getSession();
		Integer uidx = (Integer) session.getAttribute("uindex");
		try {
//			uidx=8003;
			if(uidx != null){
				int pidx = Integer.parseInt(request.getParameter("edit-idx"));
				//String playlist = request.getParameter("editname");
				String playlist = new String(request.getParameter("editname").getBytes("8859_1"),"UTF-8");

				if (playlist.length() < 11) db.editPlaylist(uidx, pidx, playlist); //세션으로
				response.sendRedirect("Mylist.do");
			}else{
				response.sendRedirect("");
			}
			
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			db.Close();
		}
	}

}
