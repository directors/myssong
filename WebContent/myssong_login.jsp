<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <title>로그인</title>
	<link href="css/rating_songs.css" rel="stylesheet" />
    <link href="css/application.min.css" rel="stylesheet" />
    <link rel="shortcut icon" href="img/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta charset="utf-8" />
    <script src="lib/jquery/jquery.1.9.0.min.js"></script>
    <script src="lib/backbone/underscore-min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/sha1.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script>
		/**
		* toast function
		*/
		var intervalCounter = 0;

		function hideToast() {
			var alert = document.getElementById("toast");
			alert.style.opacity = 0;

		}
		function drawToast(message) {

			var alert = document.getElementById("toast");

			if (alert == null) {
				var toastHTML = '<div id="toast">' + message + '</div>';
				document.body.insertAdjacentHTML('beforeEnd', toastHTML);
			} else {
				alert.innerText = message;
				alert.style.opacity = .9;
			}
			intervalCounter = setInterval("hideToast()", 3000);
		}
		
		function getPasswordSha1() {
			var orignalPW = document.loginform.password.value;
			var hashedPW = hex_sha1(orignalPW);
			//alert(hashedPW);
			return hashedPW;
			//document.loginform.action="board.write.php";
			//document.loginform.submit();
		}
		function onSend() {
			var id = $('#userid').val();
			var passwd = getPasswordSha1();
			$('#passwordsha1').val(passwd);
			$('#password').val('');
			document.loginform.submit();
			return false;
		}
	</script>
</head>
<body>
<div class="single-widget-container">
    <section class="widget login-widget">
        <header class="text-align-center">
            <h2>내 귀에 SSong</h2>
        </header>
        <div class="body">
            <form name="loginform" class="no-margin" action="Login.do" method="post" onsubmit="return onSend();"/>
                <fieldset>
                    <div class="control-group no-margin">
                        <label class="control-label" for="username">ID</label>
                        <div class="control">
                            <div class="input-prepend input-padding-increased">
                                <span class="add-on">
                                    <i class="icon-user icon-large"></i>
                                </span>
                                <input name="username" id="userid" type="text" placeholder="아이디 입력"/>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="password">Password</label>
                        <div class="control">
                            <div class="input-prepend input-padding-increased">
                                <span class="add-on">
                                    <i class="icon-lock icon-large"></i>
                                </span>
                                <input name="password" id="password" type="password" placeholder="비밀번호 입력"/>
                         		<input name="passwordsha1" id="passwordsha1" type="password" style="
    							display:none;"> 
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-actions">
                    <button type="submit" class="btn btn-block btn-large btn-danger">
                        <span class="small-circle"><i class="icon-caret-right"></i></span>
                        <small>Sign In</small>
                    </button>
                    <div class="forgot"><a class="forgot" href="myssong_register.html">Create an Account</a></div>
                </div>
            </form>
        </div>
        <!-- <footer>
            <div class="facebook-login">
                <a href="index.html"><span><i class="icon-facebook-sign icon-large"></i> LogIn with Facebook</span></a>
            </div>
        </footer> -->
    </section>
</div>
<script>
	<c:if test="${register}">
	setTimeout("drawToast('회원가입에 성공하셨습니다.');", 500);
	</c:if>
</script>
</body>
</html>