<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="wrap">

	<div class="content container-fluid">
		<div class="row-fluid">
			<div class="span12">
				<h2 class="page-title">
					취향분석 <small>Visual Charts & Graphs</small>
				</h2>
			</div>
		</div>
		<br />
		<div class="row-fluid box-row">
			<div class="span2">
				<div class="box">
					<div class="icon">
						<i class="icon-file-alt"></i>
					</div>
					<div class="description">
						<strong>평점</strong> 준 노래 갯수
					</div>
				</div>
			</div>
			<div class="span2">
				<div class="box">
					<div class="big-text">${RATINGCOUNT}개</div>
					<div class="description">
						<i class="icon-user"></i>
						<c:out value='${NICKNAME}' />
					</div>
				</div>
			</div>
			<div class="span2">
				<div class="box">
					<div class="icon">
						<i class="icon-music"></i>
					</div>
					<div class="description">
						총 들어본 <strong>노래</strong> 갯수
					</div>
				</div>
			</div>

			<div class="span2">
				<div class="box">
					<div class="big-text">${SONGCOUNT}개</div>
					<div class="description">
						<i class=" icon-play-circle"></i> 들었습니다.
					</div>
				</div>
			</div>
			<div class="span2">
				<div class="box">
					<div class="icon">
						<i class="icon-inbox"></i>
					</div>
					<div class="description">
						리스트에 담은 <strong>노래</strong> 갯수
					</div>
				</div>
			</div>
			<div class="span2">
				<div class="box">
					<div class="big-text">${LISTCOUNT}개</div>
					<div class="description">
						<i class="icon-arrow-right"></i> 담았습니다.
					</div>
				</div>
			</div>
		</div>
		<br /> <br />
		<div class="row-fluid">
			<div class="span4">
				<section class="widget">
					<header>
						<h4>
							<i class="icon-list-ol"></i> 선호가수
						</h4>
					</header>
					<div class="body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th class="hidden-phone-portrait">#</th>
									<th>가수</th>
									<th>선호도</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="artistarr" items="${ARTISTRANK}"
									varStatus="counter">
									<tr>
										<td>${counter.count}</td>
										<td><a
											href="detailSearch.do?query=${artistarr.artistname}&type=ARTISTMUSIC&page=1">${artistarr.artistname}</a></td>
										<td class="hidden-phone-portrait">${artistarr.preferCount}</td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</div>
				</section>
			</div>
			<div class="span3">
				<section class="widget large" style="height: 370px">
					<header>
						<h4>
							<i class="icon-circle-arrow-right"></i> 선호하는 장르
						</h4>
					</header>
					<div class="body">
						<div id="sources-chart-pie" class="chart pie-chart">
							<svg></svg>
						</div>
					</div>
					<footer id="data-chart-footer" class="pie-chart-footer"> </footer>
				</section>
			</div>

			<div class="span5">
				<section class="widget">
					<header>
						<h4>
							<i class="icon-bar-chart"></i> 선호하는 년대
						</h4>
					</header>
					<div class="body">
						<c:forEach var="datearr" items="${DATERANKLIST}"
							varStatus="counte">
							<c:if test="${datearr.dateYear != -1 }">
								<h5 class="no-margin weight-normal">${datearr.dateYear}년대</h5>


								<c:if test="${counte.count%4 == 1}">
									<div class="progress">
								</c:if>
								<c:if test="${counte.count%4 == 2}">
									<div class="progress progress-warning">
								</c:if>
								<c:if test="${counte.count%4 == 3}">
									<div class="progress progress-danger">
								</c:if>
								<c:if test="${counte.count%4 == 0}">
									<div class="progress progress-inverse">
								</c:if>

								<div class="bar"
									style="width: ${datearr.dateCount/DATERANKLIST[fn:length(DATERANKLIST)-1].dateCount*100}%;"></div>
								<span> </span>  ${datearr.dateCount}
					
					</div>
					</c:if>
					</c:forEach>
				</section>
			</div>
		</div>
		<div>
			<div class="row-fluid">
				<div class="span5">

					<section class="widget">
						<header>
							<h4>
								<i class="icon-heart"></i> 선호 곡들
							</h4>
						</header>
						<div class="body">
							<table class="table table-striped">
								<thead>
									<tr>
										<th class="hidden-phone-portrait">#</th>
										<th>가수</th>
										<th>곡 명</th>
										<th>선호도</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="songsarr" items="${SONGSRANK}"
										varStatus="countr">
										<tr>
											<td>${countr.count}</td>
											<td><a
												href="detailSearch.do?query=${songsarr.artist}&type=ARTISTMUSIC&page=1">${songsarr.artist}</a></td>
											<td class="hidden-phone-portrait"><a
												href="detailSearch.do?query=${songsarr.title}&type=MUSIC&page=1">${songsarr.title}</a></td>
											<td class="hidden-phone-portrait">${songsarr.rating}</td>
										</tr>
									</c:forEach>

								</tbody>
							</table>
						</div>
					</section>
				</div>
				<c:if test="${FAVORITEUSER!= null }"> 
				<div class="span5">
					<section class="widget">
						<header>
							<h4>
								<i class="icon-thumbs-up"></i> " <c:out value='${NICKNAME}' /> "님과 가장 유사한 사용자
							</h4>
						</header>
						<div class="body">
							<div class="progress progress-warning progress-striped active">
								<div class="bar" style="width: ${FAVORITEUSER.favoritescore}%;"></div>
								<span> </span> ${FAVORITEUSER.favoritescore}
							</div>
							<div class="offset4">
								<h4 class="no-margin weight-normal">
									  <i class="icon-user"></i> <a href="UserChart.do?index=${FAVORITEUSER.favoriteuidx}" class="link">${FAVORITEUSER.userNickname}</a>
								</h4>
							</div>


						</div>
					</section>
				</div>
 </c:if> 
			</div>

		</div>

	</div>