function addList()
{
	var listname = $('#listname').val();
	if (listname == "")
	{
		alert('리스트 이름을 입력하세요');
		return;
	}
	if (listname.length > 10)
	{
		alert('리스트 이름이 너무 깁니다');
		return;
	}
	document.frm_add.submit();
}

function removeList()
{
	if (!confirm("정말 삭제하시겠습니까?"))
	{
	    return;
	}
	
	pidx = ($('#listTab li.active').attr('id')).substr(5);
	var ptitle = $('#cate-' + pidx + '>input').attr('value');
	
	if (ptitle == '기본리스트')
	{
		alert('기본리스트는 삭제 할 수 없습니다.');
		return;
	}
	
	$('#remove-idx').attr('value', pidx);
	
	document.frm_remove.submit();
}

function editList()
{
	var listname = $('#editname').val();
	if (listname == "")
	{
		alert('리스트 이름을 입력하세요');
		return;
	}
	if (listname.length > 10)
	{
		alert('리스트 이름이 너무 깁니다');
		return;
	}
	
	pidx = ($('#listTab li.active').attr('id')).substr(5);
	var ptitle = $('#cate-' + pidx + '>input').attr('value');
	
	if (ptitle == '기본리스트')
	{
		alert('기본리스트는 편집 할 수 없습니다.');
		return;
	}
	
	$('#edit-idx').attr('value', pidx);

	document.frm_edit.submit();
}

function setEditList()
{
	var pidx = ($('#listTab li.active').attr('id')).substr(5);
	var ptitle = $('#cate-' + pidx + '>input').attr('value');
	
	$('#editname').val(ptitle);
}

function checkAll(classname, state)
{
	$('.' + classname).attr('checked', state);
	if(state==true)
		$('.' + classname).parent().addClass('checked');
	else
		$('.' + classname).parent().removeClass('checked');
}

function checkAll2(element, classname)
{
	var state;
	if (element.innerText == ' 전체 선택')
	{
		element.innerHTML = '<i class="icon-ok"></i> 전체 해제';
		state = true;
	}
	else
	{
		element.innerHTML = '<i class="icon-ok"></i> 전체 선택';
		state = false;
	}
	$('.' + classname).attr('checked', state);
	if(state==true)
		$('.' + classname).parent().addClass('checked');
	else
		$('.' + classname).parent().removeClass('checked');
}

function playSelect(classname)
{
	$('.' + classname).each(function() {
		if ($(this).attr('checked') == 'checked')
		{
			var iidx = ($(this).attr('id')).substr(6);
			var midx = $('#div-p-' + iidx + ' .midx').attr('value');
			var martist = $('#div-p-' + iidx + ' .artist').attr('value');
			var mtitle = $('#div-p-' + iidx + ' .title').attr('value');
			//
			playYoutube({midx:midx,artist:martist,title:mtitle});
		}
	});
}

function removeSelect(classname)
{
	var selcnt = 0;
	var iidx;
	$('.' + classname).each(function() {
		if ($(this).attr('checked') == 'checked')
		{
			++selcnt;

			iidx = ($(this).attr('id')).substr(6);
		}
	});
	if (selcnt > 1)
	{
		alert('하나씩만 삭제할 수 있습니다');
		return;
	}
	if (selcnt == 1)
	{
		if (!confirm("정말 삭제하시겠습니까?"))
		{
		    return;
		}
		$('#songremove-idx').attr('value', iidx);
		document.frm_songremove.submit();
	}
}