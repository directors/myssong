/**
 * 재생목록 추가 버튼 누를 경우 
 */
var evt ='';
var addMainMusicMidx = '';
var currentAlbumIdx='';
var musicList=[];
function clickList(event){
	evt = event;
	currentAlbumIdx = $(event.target).attr('id');
	$($('#dropdownBt')[0]).text(' '+event.target.text+' ');
	//$(evt.target).insertAfter('<i class="icon-caret-down"></i>');
	return false;
}
/**
 * 리스트에 등록될 음악 목록
 */
function getList(){
	if(addMainMusicMidx != ''){
		musicList.push(parseInt(addMainMusicMidx));
	}
	for (var i=0 ; i<$('.checked input').length ;i++ ){
		musicList.push(parseInt($($('.checked input')[i]).attr('midx')));
	}
}
function resetList(){
	$($('#dropdownBt')[0]).html('&nbsp; 나의 리스트 &nbsp;<i class="icon-caret-down"></i>');
}
function sendMusicList(){
	getList();
	resetList();
	if( musicList.length === 0) {
		alert('곡을 선택해 주세요.');
		$('#myModal2').hide();
		return;
	}
	if ((currentAlbumIdx !== '' || currentAlbumIdx.length !== 0) ) {
		data_ = {
				albumidx : currentAlbumIdx,
				musiclist : JSON.stringify(musicList)
		};
		$.ajax({
			url : "AddMusicToPlayList.do",
			type : 'POST',
			dataType : 'json',
			data : data_,
			contentType : 'application/x-www-form-urlencoded',

			success : function() {
//				alert('성공');
				musicList = [];
				addMainMusicMidx = '';
				currentAlbumIdx = '';
				drawToast("추가되었습니다~");
				$('#myModal2').hide();
			},
			error : function(data, status, er) {
//				alert("error: " + data + " status: " + status
//						+ " er:" + er);
				musicList = [];
				addMainMusicMidx = '';
				currentAlbumIdx = '';
				$('#myModal2').hide();
			}
		});

	}else{
		alert('저장할 앨범을 선택해 주세요.');
	}
}