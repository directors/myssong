/**
 * by chang
 * 반드시 this를 써서 컨텍스를 맞춰준다.(현재 인스턴스, 안쓴다면 글로벌 컨텍스트)
 * Youtube 플레이어 컨트롤러
 * (단, Youtube 플레이어 자체 리스트를 사용하지 않는다. 모두 콜백으로 처리)
 */
function Player(youtubeObj){
	this.youtubeObj = youtubeObj;	//youtube 플레이어 객체
	this.playList = [];		//MusicItem 객체 저장 리스트
	this.currentPlayItem;	//MusicItem 객체 없을경우 undefined
	this.state = -1;		//정지:-1 재생:1
	//this.videolist = [];	//오른쪽 창 비디오 리스트
}

/**
 * 재생 상태 문자열 변경
 * @param state
 */
Player.prototype.setPlayerState= function(state){
	$('#playerState').text(' '+state);
};

/**
 * 재생 상태 아이콘 변경
 * @param state
 */
Player.prototype.setPlayerStateIcon= function(src){
	$('#playerStateIcon')[0].src = src;
};

/**
 * 음악 추가
 * 중복 체크
 * @param musicItem
 */
Player.prototype.addMusic = function(musicItem){
	if(this.getIndex(musicItem.musicIndex)!==-1){
		return false;
	}
	//youtube에서 검색한 값이 들어와야 함.
	this.playList.push(musicItem);
	//리스트 invalidate
	this.invalidatePlayListAddItem(musicItem);
	//현재 재생중인 음악이 없다면 재생 시작
	/**
	 * 현재 재생중인 음악이 없거나 플레이 리스트에 아이템이 없다면
	 * 음악 추가, 최초 리스트 생성
	 */
	if(this.state == -1 && this.playList.length!=0){
		
		this.play(musicItem.musicIndex, musicItem.youtubeList[0].videoId);
	}
	return true;
};
Player.prototype.start = function(){
	if(this.playList.length > 0 ){
		this.play(this.playList[0].musicIndex,this.playList[0].youtubeList[0].videoId);
	}
};

/**
 * 새로운 인덱스 재생
 * @param musicIndex : 음악 인덱스
 * @param videoID : 재생 할 비디오 ID
 */
Player.prototype.play = function(musicIndex,videoID){
	//youtube 제어
	//alert(videoID);
	this.youtubeObj.loadVideoById(videoID,0,"medium");
	this.youtubeObj.playVideo();
	//현재 재생중인 음악 인덱스
	this.currentPlayItem = this.findMusicObject(musicIndex);
	//오른쪽 창 invalidate
	var musicItem = this.findMusicObject(musicIndex);
	this.invalidateRightWindow(musicItem,musicItem.getOtherYoutubeIDList(videoID));
	//왼족 창 invalidate
	this.invalidateNowPlay(musicItem);
	//플레이어 상태
	this.state=1;
};

/**
 * 이전 곡 재생
 * @param musicIndex
 */
Player.prototype.beforePlay = function(){
	var listIndex = this.getIndex(this.currentPlayItem.musicIndex) - 1;//다음곡
	var nextMusicItem = this.playList[listIndex];
	if(listIndex <= -1){	//플레이어 정지 
		//this.currentPlayItem = undefined;
		this.state = -1;
		return false;
	}
	//youtube 제어
	this.youtubeObj.loadVideoById(nextMusicItem.youtubeList[0].videoId,0,"medium");//this.playList[listIndex],0,"medium");
	this.youtubeObj.playVideo();
	this.currentPlayItem = nextMusicItem;
	//오른쪽창 invalidate
	this.invalidateRightWindow(nextMusicItem, 
			nextMusicItem.getOtherYoutubeIDList(nextMusicItem.youtubeList[0].videoId));
	
	//왼쪽창 invalidate
	this.invalidateNowPlay(nextMusicItem);
	this.state = 1;
	return true;
};

/**
 * 다음 곡 재생
 * @param musicIndex
 */
Player.prototype.nextPlay = function(){
	var listIndex = this.getIndex(this.currentPlayItem.musicIndex) + 1;//다음곡
	var nextMusicItem = this.playList[listIndex];
	if(listIndex >= this.playList.length){	//플레이어 정지
		if(repeatState==1){//반복 모드라면 0번 부터 시작
			nextMusicItem = this.playList[0];
		}else{
			//this.currentPlayItem = undefined;	//초기화 하지않고 계속 냅둔다. 대신 state 변수를 이용해 플레이어 상태표현
			this.state = -1;
			return false;
		}
	}
	//youtube 제어
	this.youtubeObj.loadVideoById(nextMusicItem.youtubeList[0].videoId,0,"medium");//this.playList[listIndex],0,"medium");
	this.youtubeObj.playVideo();
	this.currentPlayItem = nextMusicItem;
	//오른쪽창 invalidate
	this.invalidateRightWindow(nextMusicItem, 
			nextMusicItem.getOtherYoutubeIDList(nextMusicItem.youtubeList[0].videoId));
	
	//왼쪽창 invalidate
	this.invalidateNowPlay(nextMusicItem);
	this.state = 1;
	return true;
};
/**
 * 재생하는 동영상 변경
 * @param currentMusicIndex : 현재 재생중이던 음악 아이디
 * @param videoIndex : 재생할 다른 비디오 인덱스 (시작할 요소 안의 비디오 아이디)
 */
Player.prototype.playOtherVideo = function(currentMusicIndex, videoIndex){
	//youtube 제어
	//오른쪽 창 invalidate
};
/**
 * 리스트에서 음악 삭제 (youtube에서 재생 중인지는 고려하지 않음)
 * @param musicIndex : 삭제할 음악 인덱스
 * @returns
 */
Player.prototype.delMusic = function(musicIndex){
	var musicItem = this.findMusicObject(musicIndex);
	for (var i=0; i<this.playList.length; i++){
		if(this.playList[i] === musicItem){
			if(musicItem == this.currentPlayItem){
				if(this.nextPlay()==false){
					if(this.beforePlay()==false){
						//한곡만 남았을때 그냥 재생
					}
				}
			}
			var delItem = this.playList.splice(i,1);	//삭제
			//음악 리스트 invalidate
			this.invalidateDeleteItem(musicItem);
			//if(i==0)
			//	this.play(this.playList[0].musicIndex, this.playList[0].youtubeList[0].videoId);
			return delItem;
		}
	}
	return undefined;
};
/**
 * 음악 리스트 제거 (youtube에서 재생 중인지는 고려하지 않음)
 */
Player.prototype.delList = function(){
	this.playList = [];
	//음악 리스트 invalidate
	this.invalidateRemoveList();
};
Player.prototype.delMusicWrap = function(){
	this.delMusic(checkedMusicIndex);
};
Player.prototype.moveUpItemWrap = function(){
	this.moveUpItem(checkedMusicIndex);
};
Player.prototype.moveDownItemWrap = function(){
	this.moveDownItem(checkedMusicIndex);
};
/**
 * 음악 리스트 위로
 */
Player.prototype.moveUpItem = function(musicIndex){
	var musicItem = this.findMusicObject(musicIndex);
	var	itemIndex = this.playList.indexOf(musicItem);
	if(itemIndex !== -1 && itemIndex !== 0){ //item이 존재한다면 //가장 상단 음악이 아니라면
		//sawp을 진행한다.
		var beforeItem = itemIndex -1;
		var temp = this.playList[beforeItem];
		this.playList[beforeItem] = this.playList[itemIndex];
		this.playList[itemIndex] = temp;
		//음악 리스트 invalidatev
		this.invalidateSwapItems(this.playList[beforeItem],this.playList[itemIndex] );

	}
	//음악 리스트 invalidate
};
/**
 * 
 * @param musicIndex : 내리고 싶은 인덱스
 * @returns {Boolean}
 */
Player.prototype.moveDownItem = function(musicIndex){
	var musicItem = this.findMusicObject(musicIndex);
	var	itemIndex = this.playList.indexOf(musicItem);
	if(itemIndex !== -1){ //item이 존재한다면
		if(itemIndex===this.playList.length-1){
			return false;
		}else {	//sawp을 진행한다.
			var afterItem = itemIndex +1;
			var temp = this.playList[afterItem];
			this.playList[afterItem] = this.playList[itemIndex];
			this.playList[itemIndex] = temp;
			//음악 리스트 invalidate
			this.invalidateSwapItems(this.playList[afterItem],this.playList[itemIndex] );
		}
	}
};
/**
 * @param musicIndex : 음악 아이템 인덱스
 * @return 아이템의 인덱스 반환
 */
Player.prototype.getIndex = function(musicIndex){
	var musicItem = this.findMusicObject(musicIndex);
	return this.playList.indexOf(musicItem);
};
/**
 * MusicItem 인스턴스 반환
 * @param musicIndex
 * @returns
 */
Player.prototype.findMusicObject = function(musicIndex){
	for(var i=0; i < this.playList.length; i++){
		if(this.playList[i].musicIndex === musicIndex){
			return this.playList[i];
		}
	}
	return undefined;
};

/**
 * 현재 재생 아이콘
 * @param musicItem
 */
Player.prototype.invalidateNowPlay = function(musicItem){
	$(".nowplay").remove();
	$("div #"+musicItem.musicIndex).append($("<div style=\"text-align:right\" class=\"nowplay\"><img id=\"playerStateIcon\" src =\"img/nowplay.gif\"/><strong><small id=\"playerState\"> </small></strong></div>"));
};

/**
 * 재생 목록 변경
 * @param musicItem
 */
Player.prototype.invalidatePlayListAddItem = function(musicItem){//checkbox_radio.js 의존
	var itemTag = $('<div class="list-group-item">\
			<input id="'+musicItem.musicIndex+'" style="margin-right:5px"\
			onclick="' + 'checkMusic('+musicItem.musicIndex+')' + ';" type="checkbox">\
			<a href="#" onclick="alert(\'hello\');">\
	<span class="glyphicon glyphicon-music"></span><span class="text">넬 - 기억을 걷는 시간 </span>\
			</a></div>');
	itemTag.attr('id',musicItem['musicIndex']);
	itemTag.find('.text').text(" "+musicItem['artist'] +' - '+musicItem['title']);
	itemTag.find('a').attr('onclick','controller.play('+musicItem.musicIndex+',\''+musicItem.youtubeList[0]['videoId']+'\');');	//버튼 누를 경우 플레이
	
	$('.playlist.list-group').append(itemTag);
};

/**
 * 태그 스왑
 * @param musicItemTarget1
 * @param musicItemTarget2
 */
Player.prototype.invalidateSwapItems = function(musicItemTarget1, musicItemTarget2){
	var targetTag1 = $('.playlist.list-group').find('#'+musicItemTarget1.musicIndex);
	var targetTag2 = $('.playlist.list-group').find('#'+musicItemTarget2.musicIndex);
	if(this.getIndex(musicItemTarget1.musicIndex) > this.getIndex(musicItemTarget2.musicIndex)){
		targetTag2.insertBefore(targetTag1);	//스왑
	}else{
		targetTag1.insertBefore(targetTag2);	//스왑
	}
};
/**
 * 재생 목록 특정 아이템 삭제
 * @param musicItem
 */
Player.prototype.invalidateDeleteItem = function(musicItem){
	$('.playlist.list-group').find('#'+musicItem.musicIndex).remove();
};

/**
 * 리스트를 지움
 */
Player.prototype.invalidateRemoveList = function(){
	$('.playlist.list-group').empty();
	$('.playlist.list-group').append($('<span class="list-group-item">재생 목록</span>'));
};
/**
 * 오른쪽 윈도우에 검색 결과 추가
 * @param musicItem
 * @param videoList
 */
Player.prototype.invalidateRightWindow = function(musicItem,videoList){
	$('.list-group.videolist').empty();
	$('.list-group.videolist').append($('<span class="list-group-item">검색 결과</span>'));
	for(var i=0; i<videoList.length; i++){
		var videoItemTag = $('<a href="#" class="list-group-item"> <span class="glyphicon glyphicon-chevron-right">\
				<img style="width:85%" src="http://img.naver.net/static/newsstand/up/2014/0715/293.gif">\
				</span> <span class="blockType">옐로우 몬스터즈 - Destruction</span>\
		</a>');
		videoItemTag.find('img').attr('src',videoList[i]['imageSrc']);
		videoItemTag.find('.blockType').text(videoList[i]['title']);
		videoItemTag.attr('onclick', 'controller.play('+musicItem['musicIndex']+',\''+videoList[i]['videoId']+'\');');
		$('.list-group.videolist').append(videoItemTag);
	}
};

var repeatState=-1;// -1: off, 1: on
function invalidateRepeatButton(){
	repeatState *= -1;
	if(repeatState==-1){
		//on
		$('#repeatButton').css('color','#dddddd');
	}else{
		//off
		$('#repeatButton').css('color','');
	}
}