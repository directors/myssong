/**
 * 
 * @param artist
 * @param song
 */

function searchVideo(musicIndex,artist,song){
	var query = artist +' '+ song;
	var getParameter = {
			part:'snippet',
			q:query,
			key:'AIzaSyD9bIK_mXcDHcNlfFCoqgHw4Y3To-L9LLI',
			maxResults:10
	};
	$.ajax({
		type:'get',
		url:'https://www.googleapis.com/youtube/v3/search',
		data:getParameter
	}).done(function(videoResult){
		if(videoResult.items.length==0){
			var getParameter = {	//song으로만 검색 한번 더
					part:'snippet',
					q:song,
					key:'AIzaSyD9bIK_mXcDHcNlfFCoqgHw4Y3To-L9LLI',
					maxResults:10
			};
			$.ajax({
				type:'get',
				url:'https://www.googleapis.com/youtube/v3/search',
				data:getParameter
			}).done(function(videoResult){
				if(videoResult.items.length==0){
					alert('Youtube에 데이터가 존재 하지 않습니다.');
				}else{
					completeAndAddMusic(musicIndex,videoResult,artist,song);
				}
			});
		}else{
			completeAndAddMusic(musicIndex,videoResult,artist,song);
		}
	});
};
/**
 * 재생목록에 새로운 곡을 추가한다.
 * @param videoJson
 */
function completeAndAddMusic(musicIndex,videoJson,artist,song){
	var youtubeList = makeYoutubeItems(videoJson);
	var searchResult = new MusicItem(musicIndex,song,artist,undefined,undefined,youtubeList);
	controller.addMusic(searchResult);
}