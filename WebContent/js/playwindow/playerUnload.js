

/**
 * 테스트용 alert은 동작하지 않는다. return 으로 확인한다.
 */
if (window.attachEvent) {
    /*IE and Opera*/
    window.attachEvent("onunload", function() {
    	removeCookie('changwon');
    	window.opener.childWindow = '';
    	//return "onbefore";
        /* ......?!@# */
    });
} else if (document.addEventListener) {
    /*Chrome, FireFox*/
    window.onbeforeunload = function() {
    	removeCookie('changwon');
    	window.opener.childWindow = '';
    	//return "onbefore";
        /* ......?!@# */
    };
    /*IE 6, Mobile Safari, Chrome Mobile*/
    window.addEventListener("unload", function() {
    	removeCookie('changwon');
    	window.opener.childWindow = '';
    	//return "onbefore";
        /* ......?!@# */
    }, false);
} else {
    /*etc*/
    document.addEventListener("unload", function() {
    	removeCookie('changwon');
    	window.opener.childWindow = '';
    	//return "onbefore";
        /* ......?!@# */
    }, false);
}
