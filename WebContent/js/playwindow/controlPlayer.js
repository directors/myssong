/**
 * 플레이어 창을 만들고 제어할 수 있게한다.
 */
var childWindow = '';
var reserveList = [];
function playYoutube(musicItem){
	//alert(musicItem);
	var cookieContent = getCookie('changwon');
	if(cookieContent === ''){
		if( childWindow === ''){
			//재생목록 추가
			if(musicItem==undefined){
				for(var i=0 ; i<$('.checked input').length;i++){
					var artist= $($('.checked input')[i]).attr('artist');
					var title= $($('.checked input')[i]).attr('title');
					var mitdx= $($('.checked input')[i]).attr('midx');
					//childWindow
					var dict_ = {midx:parseInt(mitdx), artist:artist,title:title};
					reserveList.push(dict_);
				}
			}
			if(musicItem != undefined){
			//	alert('hello');
				var dict_ = {midx:parseInt(musicItem.midx), artist:musicItem.artist,title:musicItem.title};
				reserveList.push(dict_);
			}
			childWindow = window.open('Player.do','changwon',"width=750,height=541,resizable=no,scrollbars=no");
		//	setCookie('changwon','changwon',30);
		}else{
			//쿠키가 없는상태에서(플레이어 로딩 중) 음악을 추가할 경우(0829)
			if(musicItem !== undefined){
				var dict_ = {midx:parseInt(musicItem.midx), artist:musicItem.artist,title:musicItem.title};
				if(childWindow.controller==undefined){
				//	alert('childWindow undefined');
					reserveList.push(dict_);		//controller가 아직 없을때
				//	alert('undefined');
				}else{								//controller가 아직 없을때
				//	alert('childWindow Exists');
					childWindow.searchVideo(parseInt(musicItem.midx),musicItem.artist,musicItem.title);
				//	alert('addmusic');//search 해야함
				}
			}
			//쿠키가 없는 상태에서 갱신 불가능
			//setCookie('changwon','changwon',30);//쿠키 갱신
		}

		//쿠키 생성
	}else{
		setCookie('changwon','changwon',20);//쿠키 갱신
		if( childWindow === '')
			childWindow = window.open('','changwon');	//객체를 얻어옴
		//재생목록 추가
		if(musicItem==undefined){
			for(var i=0 ; i<$('.checked input').length;i++){
				var artist= $($('.checked input')[i]).attr('artist');
				var title= $($('.checked input')[i]).attr('title');
				var mitdx= $($('.checked input')[i]).attr('midx');
				//childWindow
				childWindow.searchVideo(parseInt(mitdx),artist,title);
			}
		}
		if(musicItem!=undefined){
			if(childWindow.searchVideo==undefined){
				var dict_ = {midx:parseInt(musicItem.midx), artist:musicItem.artist,title:musicItem.title};
				reserveList.push(dict_);
			}else{
				childWindow.searchVideo(parseInt(musicItem.midx),musicItem.artist,musicItem.title);
			}
		}

	}
	drawToast("재생목록에 추가되었습니다.");
	return false;
}
//쿠키 삭제
function removeCookie(cName){
	setCookie(cName,'',-99999999);
}
//쿠키 생성
function setCookie(cName, cValue, cMinutes){
	var expire = new Date();
	expire.setMinutes(expire.getMinutes() + cMinutes);
	cookies = cName + '=' + escape(cValue) + '; path=/ '; 
	if(typeof cMinutes != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
	document.cookie = cookies;
}
//쿠키 가져오기
function getCookie(cName) {
	cName = cName + '=';
	var cookieData = document.cookie;
	var start = cookieData.indexOf(cName);
	var cValue = '';
	if(start != -1){
		start += cName.length;
		var end = cookieData.indexOf(';', start);
		if(end == -1)end = cookieData.length;
		cValue = cookieData.substring(start, end);
	}
	return unescape(cValue);
}