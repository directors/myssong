/**
 * by chang
 * 음악 아이템을 정의하는 스크립트
 */
/**
 * 재생 리스트에 들어갈 클래스
 * 생성자
 * youtubeList : 5개의 검색된 영상(YoutubeItem 클래스)
 */
function MusicItem(musicIndex, title, artist, albumName, albumImageSrc, youtubeList){
	this.musicIndex = musicIndex;
	this.title = title;
	this.artist = artist;
	this.albumName = albumName;//deprecated
	this.albumImageSrc = albumImageSrc;//deprecated
	this.youtubeList = youtubeList;
}
/**
 * @param index "except this parameter in result to be returned"
 * @returns
 */
MusicItem.prototype.getOtherYoutubeIDList = function(youtubeID){
	var youtubeItemObject;
	for(var i=0; i<this.youtubeList.length; i++){
		if(this.youtubeList[i]['videoId']===youtubeID){
			youtubeItemObject = this.youtubeList[i];
		}
	}
	if(youtubeItemObject==undefined)
		return false;
	var index = this.youtubeList.indexOf(youtubeItemObject);
	var front = this.youtubeList.slice(0,index);
	var back = this.youtubeList.slice(index+1,this.youtubeList.length);
	for(var i=0; i<back.length; i++){
		front.push(back[i]);
	}
	return front;
};
/**
 * title : 동영상 제목
 * src : 동영상 아이디
 */
function YoutubeItem(title,videoId,imageSrc){
	this.title = title;
	this.videoId = videoId;
	this.imageSrc = imageSrc;
}
/**
 * 검색 결과로 부터 Youtube 객체를 생성해주는 함수
 * @param searchData : 검색결과(JSON)
 */
function makeYoutubeItems(searchData){
	var videoList=[];
	var itemList = searchData.items;
	for (var i=0; i<itemList.length; i++){
		mItem = itemList[i];
		var videoId = mItem.id.videoId;
		if(videoId==undefined){
			continue;
		}
		var videoTitle = mItem.snippet.title;
		var videoImg = mItem.snippet.thumbnails.medium.url;
		videoList.push(new YoutubeItem(videoTitle,videoId,videoImg));
	}
	return videoList;
};