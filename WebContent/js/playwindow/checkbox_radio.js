/**
 * 
 */
var checkedMusicIndex = undefined;
function checkMusic(musicIndex){
	//alert('checkMusic');
	if(checkedMusicIndex == musicIndex){	//이전에 체크 되었던 박스일 경우 아무것도 disable하지 않고 반환한다.
		return;
	}
	if(checkedMusicIndex){
		//disable invalidate
		invalidateDisableCheckBox();
	}
	checkedMusicIndex = musicIndex;
}

function invalidateDisableCheckBox(){
	if(checkedMusicIndex){
		$('#'+checkedMusicIndex+' input').attr('checked',false);
	}
}