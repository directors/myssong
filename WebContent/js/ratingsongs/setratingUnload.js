/**
 * 테스트용 alert은 동작하지 않는다. return 으로 확인한다.
 */
if (window.attachEvent) {
	/* IE and Opera */
	window.attachEvent("onunload", function() {
		
		ratingsongsDB();
	});
} else if (document.addEventListener) {
	/* Chrome, FireFox */
	window.onbeforeunload = function() {
		
		ratingsongsDB();
	};
	/* IE 6, Mobile Safari, Chrome Mobile */
	window.addEventListener("unload", function() {
		
		ratingsongsDB();
	}, false);
} else {
	/* etc */
	document.addEventListener("unload", function() {
		
		ratingsongsDB();
	}, false);
}
function ratingsongsDB() {
	//alert('setdb2');
	$.ajax({
		url : "RatingSong.do",
		type : 'POST',
		dataType : 'json',
		async : false,
		data : {
			ratinglist : JSON.stringify(ratinglist),
			predictlist : JSON.stringify(predictList)
		},
		contentType : 'application/x-www-form-urlencoded',

		success : function() {
			//alert('rating ajax success' + ratinglist);
			//window.location = "RatingSong.do";
		},
		error : function(data, status, er) {
			//alert('rating ajax false' + ratinglist);
			//alert("error: " + data + " status: " + status + " er:" + er);
		}
	});
}
