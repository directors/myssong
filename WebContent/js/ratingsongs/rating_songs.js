/**
 * 
 */
function dbsetter(midx, rating) {
	this.midx = midx;
	this.rating = rating;
}
/*var thisrating;*/

var ratinglist = [];
var songchecklist = [];

function barupdate(length) {

	var currValue = parseInt($("#progressbar").data("value")) + length;
	currValue = parseInt(currValue) ? parseInt(currValue) : 0;
	$("#progressbar").progressbar({
		value : currValue
	}).data("value", currValue);
	if (((currValue) / 30 * 100) < 100) {
		$("#progressbar").css("display", "block");
		$("#progressbar").css("width", ((currValue) / 30 * 100) + "%");
	}
	else if(currValue <1)
		$("#progressbar").css("display", "none");
	else if(((currValue) / 30 * 100) >= 100) {
		$("#progressbar").css("display", "block");
		$("#progressbar").css("width", 100 + "%");
		$(document).ready(function() {$('.finishbtn').css("display","block")});
	}
	$("#progressbar").text((currValue) + "개");
	
}
function setdb(finish) {
	data_ = {
			ratinglist : JSON.stringify(ratinglist)
	};
	if(finish!=undefined){
		data_.finish=true;
	}
	$.ajax({
		url : "FirstRatingSvl",
		type : 'POST',
		dataType : 'json',
		data : data_
		,
		contentType : 'application/x-www-form-urlencoded',

		success : function() {
			window.location = "firstRating.do";
		},
		error : function(data, status, er) {
//			alert("error: " + data + " status: " + status + " er:" + er);
		}
	});

}

function addRaty(idname, start, iscancel)
{
	iscancel = typeof iscancel !== 'undefined' ? iscancel : true;
	$('#' + idname)
	.raty(
			{
				score: start,
				half : true,
				cancelOff : 'cancel-off.png',
				cancelOn : 'cancel-on.png',
				starHalf : 'rating_blank.png',
				starOff : 'rating_base.png',
				starOn : 'rating.png',
				precision : true,
				click : function(score, evt) {
					if (score != null) {
						if(score<0.5){
							score = 0.5;
						}else if(score<=1.0){
							score = 1;
						}else if(score%parseInt(score)>0.5){
							score = parseInt(score)+1;
						}else{
							score = parseInt(score)+0.5;
						}
						if (songchecklist.indexOf(this.id) != -1) {
							ratinglist.splice(songchecklist
									.indexOf(this.id), 1);
							songchecklist.splice(songchecklist
									.indexOf(this.id), 1);
						}
						var item;
						if(this.id=='fixed2'){
							item = new dbsetter(fix2, score);
						}else
							item = new dbsetter(this.id, score);
						ratinglist.push(item);
						songchecklist.push(this.id);
						drawToast("수정되었습니다~");

					} else {
						if (songchecklist.indexOf(this.id) != -1) {
							ratinglist.splice(songchecklist
									.indexOf(this.id), 1);
							songchecklist.splice(songchecklist
									.indexOf(this.id), 1);
						}
					}

					

				},
				round : { down: .001, full: .000, up: .500 } ,
				hints : [
						[ 'bad 1', 'bad 2', 'bad 3', 'bad 4', 'bad 5',
								'bad 6', 'bad 7', 'bad 8', 'bad 9',
								'bad' ],
						[ 'poor 1', 'poor 2', 'poor 3', 'poor 4',
								'poor 5', 'poor 6', 'poor 7', 'poor 8',
								'poor 9', 'poor' ],
						[ 'regular 1', 'regular 2', 'regular 3',
								'regular 4', 'regular 5', 'regular 6',
								'regular 7', 'regular 8', 'regular 9',
								'regular' ],
						[ 'good 1', 'good 2', 'good 3', 'good 4',
								'good 5', 'good 6', 'good 7', 'good 8',
								'good 9', 'good' ],
						[ 'gorgeous 1', 'gorgeous 2', 'gorgeous 3',
								'gorgeous 4', 'gorgeous 5',
								'gorgeous 6', 'gorgeous 7',
								'gorgeous 8', 'gorgeous 9', 'gorgeous' ] ]

	});	
}
$.fn.raty.defaults.path = 'img';
$(function() {
	/*$('#default').raty();
*/
	$('.ratingType')
			.raty(
					{
						half : true,
						cancel : true,
						cancelOff : 'cancel-off.png',
						cancelOn : 'cancel-on.png',
						starHalf : 'rating_blank.png',
						starOff : 'rating_base.png',
						starOn : 'rating.png',
						precision : true,
						space: false,
						click : function(score, evt) {
							if (score != null) {
								if(score<0.5){
									score = 0.5;
								}else if(score<=1.0){
									score = 1;
								}else if(score%parseInt(score)>0.5){
									score = parseInt(score)+1;
								}else{
									score = parseInt(score)+0.5;
								}
								
								if (songchecklist.indexOf(this.id) != -1) {
									ratinglist.splice(songchecklist
											.indexOf(this.id), 1);
									songchecklist.splice(songchecklist
											.indexOf(this.id), 1);
									if($("#progressbar").length!=0)
										barupdate(-1);

								}
								var item = new dbsetter(this.id, score);
								ratinglist.push(item);
								songchecklist.push(this.id);
								if($("#progressbar").length!=0)
									barupdate(1);
								drawToast("저장되었습니다~");
								/*thisrating=this;*/
								$(this).parent().css("display", "block");

							} else {
								if (songchecklist.indexOf(this.id) != -1) {
									ratinglist.splice(songchecklist
											.indexOf(this.id), 1);
									songchecklist.splice(songchecklist
											.indexOf(this.id), 1);
									if($("#progressbar").length!=0)
										barupdate(-1);
								}
								drawToast("취소되었습니다~");
								$(this).parent().css("display", "");
							}
							
							

						},
						round : { down: .001, full: .000, up: .500 } ,
						hints : [
								[ 'bad 1', 'bad 2', 'bad 3', 'bad 4', 'bad 5',
										'bad 6', 'bad 7', 'bad 8', 'bad 9',
										'bad' ],
								[ 'poor 1', 'poor 2', 'poor 3', 'poor 4',
										'poor 5', 'poor 6', 'poor 7', 'poor 8',
										'poor 9', 'poor' ],
								[ 'regular 1', 'regular 2', 'regular 3',
										'regular 4', 'regular 5', 'regular 6',
										'regular 7', 'regular 8', 'regular 9',
										'regular' ],
								[ 'good 1', 'good 2', 'good 3', 'good 4',
										'good 5', 'good 6', 'good 7', 'good 8',
										'good 9', 'good' ],
								[ 'gorgeous 1', 'gorgeous 2', 'gorgeous 3',
										'gorgeous 4', 'gorgeous 5',
										'gorgeous 6', 'gorgeous 7',
							
										'gorgeous 8', 'gorgeous 9', 'gorgeous' ] ]

					});
});
var intervalCounter = 0;

function hideToast(){
	var alert = document.getElementById("toast");
	alert.style.opacity = 0;
 
}
function drawToast(message){
	
	var alert = document.getElementById("toast");
	
	if (alert == null){
		var toastHTML = '<div id="toast">' + message + '</div>';
		document.body.insertAdjacentHTML('beforeEnd', toastHTML);
	}
	else{
		alert.innerText=message;
		alert.style.opacity = .9;
	}
	
	
	intervalCounter = setInterval("hideToast()", 3000);
}
