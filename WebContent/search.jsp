<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="row-fluid">
	<div class="span10 offset1">
		<h2 class="page-title">
			결과화면 <small>artist and song</small>
		</h2>
	</div>
</div>
<div class="row-fluid">
	<div class="span8 offset2">
		<section class="widget">
			<div class="body no-margin">
				<form method="get" accept-charset="utf-8" action="search.do"
					class="form-search no-margin text-align-center" />
				<input type="search" class="span10" name="query"
					placeholder="Keywords: fx, exo" />
				<button class="btn btn-primary">Search</button>
				</form>
			</div>
		</section>
	</div>
</div>
<div class="row-fluid">
	<div class="span10 offset1">
		<section class="widget padding-increased">
			<header>
				<h4>
					<i class="icon-list"></i> 가수 검색 결과
				</h4>
				<div class="actions">
					<button class="btn btn-small btn-inverse" onclick="location.href='detailSearch.do?query=${fn:replace(QUERY,"&","%26")}&type=ARTIST&page=1';">
						<i class="icon-arrow-right"></i> View more
					</button>
				</div>
				<hr>
			</header>
			<div class="body no-margin">
				<c:forEach var="artist" items="${ARTISTLIST}" varStatus="counter">
					<c:if test="${counter.count % 4 == 1}">
						<ul class="thumbnails ">
					</c:if>
					<li class="span3 ">
						<div class="thumbnail">
							<img class="albumjacket" src="img/${artist.imagePath}" alt="" />
							<div class="bottom">
								<h4><a href="detailSearch.do?query=${fn:replace(artist.artist,"&","%26")}&type=ARTISTMUSIC&page=1">${artist.artist}</a></h4>
							</div>
						</div>
					</li>
					<c:if test="${counter.count % 4 == 0 }">
						</ul>
					</c:if>
				</c:forEach>
			</div>
		</section>
	</div>
	<div class="row-fluid">
		<div class="span10 offset1">
			<section class="widget padding-increased">
				<header>
					<h4>
						<i class=" icon-list"></i> 곡 검색 결과
					</h4>

					<div class="actions">
						<button class="btn btn-small btn-inverse" onclick="checkAll2(this, 'change-password')">
							<i class="icon-ok"></i> 전체 선택
						</button>
						<button class="btn btn-small btn-warning" onclick="playYoutube();">
							<i class="fa fa-youtube-play"></i> 재생
						</button>
						<button type="button" class="btn btn-small btn-primary" 
			data-toggle="modal" data-target="#myModal2"><i class="eicon-plus-circled"></i>음악 서랍장에 추가</button>

						<button class="btn btn-small btn-inverse" onclick="location.href='detailSearch.do?query=${fn:replace(QUERY,"&","%26")}&type=MUSIC&page=1';">
							<i class="icon-arrow-right"></i> View more
						</button>
					</div>
					<hr>
				</header>
				<c:forEach var="musics" items="${MUSICLIST}" varStatus="counter">
					<c:if test="${counter.count % 4 == 1}">
						<ul class="thumbnails ">
					</c:if>
					<li class="span3 ">
						<div class="thumbnail" id="${musics.midx}">
							<img class="albumjacket" src="img/${musics.imagePath}" alt="" />
							<label class="checkbox pull-right">
								<input type="checkbox"
										id="change-password" name="change-password" class="change-password"
										artist="${musics.artist}" title="${musics.title}"
										midx="${musics.midx}">
							</label>
							<div class="bottom">
								<h4>
									<a href="#"
										onclick="return playYoutube({midx:'${musics.midx}',artist:'${fn:replace(musics.artist,"'","\\'")}',title:'${fn:replace(musics.title,"'","\\'")}'});">${musics.artist}-${musics.title}</a>
								</h4>
							</div>

							<div class="rating">
						<div class="ratingType" style="cursor: pointer; margin-top:10px;"
							id="${musics.midx}"></div>
					</div>
						</div>
					</li>
					<c:if test="${counter.count % 4 == 0}">
						</ul>
					</c:if>
				</c:forEach>
			</section>
		</div>
	</div>
</div>