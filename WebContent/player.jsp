<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="css/player/player.css" />
<script type="text/javascript" src="js/playwindow/controlPlayer.js"></script>
<script type="text/javascript" src="js/playwindow/playerUnload.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/playwindow/player.js"></script>
<script type="text/javascript" src="js/playwindow/musicitem.js"></script>
<script type="text/javascript" src="js/playwindow/checkbox_radio.js"></script>
<script type="text/javascript" src="js/playwindow/youtubeSearch.js"></script>
<script type="text/javascript" src="js/playwindow/playersc.js"></script>
<style>
.list-group{
	box-shadow:0 1px 20px rgba(0,0,0,.075)
};
</style>
</head>
<body style="resize:none">
	<div class="entireWindow">
		<div class="leftwindow">
			<!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
			<div id="player" class="player"></div>
			<div style="margin-top: 10px; width:400px">
				<div class="list-group" style="margin-bottom:4px">
					<div class="list-group-item"><span>재생 목록</span>
					<span class="pull-right">
					<a href="#" onclick="invalidateRepeatButton();" style="
    margin-right: 10px;
"><span id="repeatButton" class="glyphicon glyphicon-repeat" style="
    color: #cccccc;
"></span></a>
					<a href="#" onclick="controller.beforePlay();"><span class="glyphicon glyphicon-backward"></span></a>
<a href="#" onclick="controller.nextPlay();" style="
    margin-right: 10px;
    margin-left: 5px;
"><span class="glyphicon glyphicon-forward"></span></a>
						<a href="#" onclick="controller.moveUpItemWrap();"><span class="glyphicon glyphicon-chevron-up"></span></a>
						<a href="#" onclick="controller.moveDownItemWrap();"><span class="glyphicon glyphicon-chevron-down" style="margin-right:5px"></span></a>
						<a href="#" onclick="controller.delMusicWrap();"><span class="glyphicon glyphicon-remove" style="color:#dd5500"></span></a>
						
					</span>
					</div>
				</div>
				<div class="list-group playlist">
					
				</div>
			</div>
		</div>
		<div style="margin-top: 5px">
			<div class="list-group videolist">
				<span class="list-group-item">검색 결과</span><a href="#"
					class="list-group-item">
				</span> <span class="blockType">리스트를 불러오는중...</span>
				</a>
			</div>
		</div>
	</div>
	<script>
		function readyCallback(){//alert('ready(setcookie)');
			setCookie('changwon','changwon',20);
		}
		//$(document).ready();
		var controller;
		// 2. This code loads the IFrame Player API code asynchronously.
		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		// 3. This function creates an <iframe> (and YouTube player)
		//    after the API code downloads.
		var player;
		function onYouTubeIframeAPIReady() {
			player = new YT.Player('player', {
				height : '240',
				width : '400',
				videoId : '',
				events : {
					'onReady' : onPlayerReady,
					'onStateChange' : onPlayerStateChange
				}
			});
			controller = new Player(player);	//Controller
		}

		// 4. The API will call this function when the video player is ready.
		function onPlayerReady(event) {
			//event.target.playVideo();
			//testCase();
			if(window.opener.reserveList!==undefined){
				for (var i=0; i<window.opener.reserveList.length; i++){
					//alert('reserve');
					searchVideo(window.opener.reserveList[i].midx,
							window.opener.reserveList[i].artist,
							window.opener.reserveList[i].title);
				}
				window.opener.reserveList.splice(0,window.opener.reserveList.length);
			}
			//controller.start();
			
			readyCallback();//플레이어 준비 끝 // 쿠키 생성
		}

		// 5. The API calls this function when the player's state changes.
		//    The function indicates that when playing a video (state=1),
		//    the player should play for six seconds and then stop.
		//	var done = false;
		var musicItemWhenFinish;
		function onPlayerStateChange(event) {
			if(event.data== YT.PlayerState.PAUSED ){
				controller.setPlayerState('paused');
				controller.setPlayerStateIcon('img/stop.jpg');
			}
			if(event.data== YT.PlayerState.BUFFERING ){
				controller.setPlayerState('buffering');
				controller.setPlayerStateIcon('img/stop.jpg');
			}
			if(event.data== YT.PlayerState.PLAYING ){
				controller.setPlayerState('now playing');
				controller.setPlayerStateIcon('img/nowplay.gif');
				
			}
			/*if (event.data == YT.PlayerState.PLAYING && !done) {
				//setTimeout(stopVideo, 6000);
				done = true;
			}*/
			if(event.data == YT.PlayerState.ENDED){
			//	alert(controller.currentPlayItem.musicIndex);
				if(controller.currentPlayItem != undefined){
					completeScore(controller.currentPlayItem.musicIndex);
					musicItemWhenFinish = controller.currentPlayItem;	//끝났을때 임시 저장
				}else{
					completeScore(musicItemWhenFinish.musicIndex);
					controller.currentPlayItem = musicItemWhenFinish;	//임시 저장된 변수 할당
				}
				controller.nextPlay();	//다음 곡 재생
				controller.setPlayerState('ended');
				controller.setPlayerStateIcon('img/stop.jpg');
			}
		}
		
		function onError(event){
			
			controller.setPlayerState('error');
			controller.setPlayerStateIcon('img/stop.jpg');
			controller.nextPlay();	//다음 곡 재생
		}
		
		function stopVideo() {
			player.stopVideo();
		}
		 function testCase(){
			searchVideo(1,'winner','끼부리지마');
			searchVideo(2,'버즈','8년만의 여름');
			searchVideo(3,'ellegarden','red hot');
		}
		 
	</script>

</body>
</html>