<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="row-fluid">
	<div class="span12 offset1">
		<h2 class="page-title">
			음악추천 <small>recommand</small>
		</h2>
	</div>
</div>
<script src="js/poem/timecontrol.js"></script>
<script type="text/javascript">

</script>
<div class="row-fluid">
	<div class="span10 offset1">
		<section class="widget">
			<header>
				<h4>
					<i class="icon-book"> 당신의 취향을 분석 중입니다. <strong id='timeid'>30</strong>초 뒤에 메인페이지로 이동합니다.</i>
				</h4>
				<hr />
			</header>

			<div class="body row-fluid">
				<div class="row-fluid">
					<div class="span4 ">
						<div class="text-align-center">
							<img class="img-circle pull-center"
								src="img/waiting.jpg" style="width: 100%;">
						</div>
						<div class="text-align-center">


						</div>
					</div>
					<div class="span8">
								<div style="margin-left:50px">
								<h3>${POEM.title}</h3>
								<b>${POEM.writter}</b><br><br>
								${POEM.poemdata}
								</div>
					</div>
				</div>
			</div>
</div>

