<%@ page language="java" contentType="text/html; charset=utf-8"
   pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>My Ear's SSONG</title>
<link href="css/application.min.css" rel="stylesheet" />
<link href="css/rating_songs.css" rel="stylesheet" />
<link rel="stylesheet" href="css/jquery.raty.css">
<link rel="shortcut icon" href="img/favicon.png" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- rating -->
<script src="lib/jquery/jquery.1.9.0.min.js"></script>
<script src="lib/jquery/jquery-migrate-1.1.0.min.js"></script>
<script type="text/javascript" src="js/jquery.raty.js"></script>
<script type="text/javascript" src="js/ratingsongs/rating_songs.js"></script>
<script type="text/javascript" src="js/ratingsongs/setratingUnload.js"></script>
<!-- basic application js-->
<script src="js/forms.js"></script>
<script src="js/playwindow/controlPlayer.js"></script>
<script src="js/playwindow/specialCharacter.js"></script>
<script src="js/mylist/additemtolist.js"></script>
<script src="js/mylist/mylist.js"></script>
<script src="js/ui-dialogs.js"></script>
</head>
<body>
   <div class="logo" style="display: block;">
      <h3>
         <a href="mainPage.do">내귀에 <strong>SSONG</strong><i
            class="icon-music"></i></a>
      </h3>
   </div>
   <div class="wrap">
      <header class="page-header">
      <div class="navbar">
         <div class="navbar-inner">
            <ul class="nav pull-right">
               <li class="divider"></li>
               <li class="drophover hidden-phone "><a href="RatingSong.do"
                  id="" class="dropdown-toggle"> <i class="icon-edit"></i>
               </a>
                  <ul id="messages-menu" class="dropdown-menu messages" role="menu">
                     <li role="presentation"><span class="details"> <span>
                              음악평점매기기</span>
                     </span></li>
                  </ul></li>
               <li class="drophover hidden-phone "><a href="Mylist.do" id=""
                  class="dropdown-toggle"> <i class="icon-folder-open"></i>
               </a>
                  <ul id="messages-menu" class="dropdown-menu messages" role="menu">
                     <li role="presentation">
                        <div class="details">
                           <div>음악서랍장</div>
                        </div>
                     </li>

                  </ul></li>


               <li class="hidden-phone dropdown"><a href="#" title="Account"
                  id="account" class="dropdown-toggle" data-toggle="dropdown"> <i
                     class="icon-user"></i>
               </a>
                  <ul id="account-menu" class="dropdown-menu account" role="menu">
                     <li role="presentation" class="account-picture">username</li>
                     <li role="presentation"><a href="UserChart.do"
                        class="link"> <i class="icon-user"></i> 취향분석
                     </a></li>
                     <li role="presentation"><a href="logout.do" class="link">
                           <i class="icon-signout"></i> logout
                     </a></li>

                  </ul></li>
               <li class="visible-phone"><a href="#" class="btn-navbar"
                  data-toggle="collapse" data-target=".sidebar" title=""> <i
                     class="icon-reorder"></i>
               </a></li>
            </ul>
            <form class="navbar-search pull-right" method="get"
               accept-charset="utf-8" action="search.do">
               <input type="search" class="search-query" placeholder="Search..."
                  name="query" />
            </form>
         </div>
      </div>
      </header>
      <div class="content container-fluid offset1">
         <!-- style="margin-left:70px;" -->
         <jsp:include page="${param.BODYNAME}" />

         <!-- PLAYLIST Modal -->
         <div id="myModal2" class="modal hide fade" tabindex="-1"
            role="dialog">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 id="myModalLabel2">앨범 저장</h4>
            </div>
            <div class="modal-body" style="min-height: 400px">
               <div class="btn-group">
                  <button class="btn btn-default btn-inverse dropdown-toggle"
                     data-toggle="dropdown" id="dropdownBt">&nbsp; 재생목록
                     &nbsp;</button>
                  <ul class="dropdown-menu">
                     <c:forEach var="listItem" items="${PLAYLIST}">
                        <li><a href="#" onclick="return clickList(event);"
                           id="${listItem.listidx }">${listItem.title }</a></li>
                     </c:forEach>
                  </ul>
               </div>
            </div>
            <div class="modal-footer">
               <button class="btn" data-dismiss="modal">닫기</button>
               <button class="btn btn-primary" data-dismiss="modal"
                  onclick="sendMusicList();">추가하기</button>
            </div>
         </div>

         <!-- PLAYLIST Modal END -->

      </div>
   </div>


   <!-- jquery and friends -->
   <script src="lib/jquery/jquery-migrate-1.1.0.min.js">
      
   </script>

   <!-- jquery plugins -->
   <script src="lib/jquery-maskedinput/jquery.maskedinput.js"></script>
   <script src="lib/parsley/parsley.js">
      
   </script>
   <script src="lib/uniform/js/jquery.uniform.js"></script>
   <script src="lib/select2.js"></script>


   <!--backbone and friends -->
   <script src="lib/backbone/underscore-min.js"></script>

   <!-- bootstrap default plugins -->
   <script src="js/bootstrap/bootstrap-transition.js"></script>
   <script src="js/bootstrap/bootstrap-collapse.js"></script>
   <script src="js/bootstrap/bootstrap-alert.js"></script>
   <script src="js/bootstrap/bootstrap-tooltip.js"></script>
   <script src="js/bootstrap/bootstrap-popover.js"></script>
   <script src="js/bootstrap/bootstrap-button.js"></script>
   <script src="js/bootstrap/bootstrap-dropdown.js"></script>
   <script src="js/bootstrap/bootstrap-modal.js"></script>
   <script src="js/bootstrap/bootstrap-tab.js"></script>

   <!-- bootstrap custom plugins -->
   <script src="lib/bootstrap-datepicker.js"></script>
   <script src="lib/bootstrap-select/bootstrap-select.js"></script>
   <script src="lib/wysihtml5/wysihtml5-0.3.0_rc2.js"></script>
   <script src="lib/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script src="js/app.js"></script>
   <script src="js/settings.js"></script>

   <script>
      $('ul.nav li.drophover').hover(
            function() {
               $(this).find('.dropdown-menu').stop(true, true).delay(200)
                     .fadeIn();
            },
            function() {
               $(this).find('.dropdown-menu').stop(true, true).delay(200)
                     .fadeOut();
            });
      /*    $.fn.raty.defaults.path = 'img';
         $('#fixed').raty({

            showHalf : true,
            readOnly : true,
            starHalf : 'rating_blank.png',
            starOff : 'rating_base.png',
            starOn : 'rating.png',
            start : 3.3
          ${FIRSTRATING.rating}
         }); */
   </script>
</body>
</html>