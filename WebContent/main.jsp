<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="row-fluid">
	<div class="span10 offset1">
		<h2 class="page-title">
			음악추천 <small>recommand</small>
		</h2>
	</div>
</div>
<div class="row-fluid">
	<div class="span10 offset1">
		<section class="widget">
			<header>
				<h4>
					<c:if test="${EXISTS == 'true'}">
						<i class="icon-list-ol"> 추천음악</i>
						<div class="pull-right">
						<button class="btn btn-small btn-success"
							onclick="window.location.reload(true);return false;">
							더 추천 받기 <i class="icon-refresh"></i>
						</button>
					</div>
					</c:if>
					<c:if test="${EXISTS == 'flase'}">
						<i class="icon-list-ol"> 추천 목록을 생성하는 중 입니다</i>
					</c:if>
					
				</h4>

				<hr />

			</header>

			<div class="body row-fluid">
				<div class="row-fluid">
					<div class="span4 ">
						<div class="text-align-center">

							<c:if test="${EXISTS == 'true'}">
								<img class="img-circle pull-center"
									src="img/${FIRSTMUSIC.imagePath}" style="width: 100%;">
							</c:if>
							<c:if test="${EXISTS == 'false'}">
								<img class="img-circle pull-center" src="img/waiting.jpg"
									style="width: 100%;">
							</c:if>
							<!-- "img/${FIRSTMUSIC.imagePath}"-->
						</div>
						<div class="text-align-center">

							<c:if test="${EXISTS == 'true'}">
								<button class="btn btn-small btn-warning"
									onclick="playYoutube({midx:'${FIRSTMUSIC.midx}',artist:'${FIRSTMUSIC.artist }',title:'${ FIRSTMUSIC.title}'});">
									<i class="icon-play-circle"></i> 재생
								</button>
								<button type="button" class="btn btn-small btn-primary"
									data-toggle="modal" data-target="#myModal2"

									onclick="addMainMusicMidx='${FIRSTMUSIC.midx}'; drawToast('추가되었습니다~');">
									<i class="eicon-plus-circled"></i> 음악 서랍장에 추가
								</button>

							</c:if>
						</div>
					</div>
					<div class="span8">
						<c:if test="${EXISTS == 'true'}">
							<h3 class="no-margin">
								<strong>${FIRSTMUSIC.artist} - ${FIRSTMUSIC.title}</strong>
							</h3>
							<h4>
								<address>
									<script>var fix2 = ${FIRSTMUSIC.midx};</script>
									<strong><div id="fixed2"></div></strong><strong style="">${FIRSTMUSIC.rating}</strong><br>
									<strong> | </strong>${FIRSTMUSIC.albumname}<strong> |
									</strong>${FIRSTMUSIC.genre}<br> <strong> | </strong>${FIRSTMUSIC.date}<br>
									<script>
									$(function() {
										addRaty('fixed2', ${FIRSTMUSIC.rating}, false);
									});
								</script>
								</address>
							</h4>
						</c:if>
						<c:if test="${EXISTS == 'false'}">
							<div style="margin-left: 50px">
								<h3>${POEM.title}</h3>
								<b>${POEM.writter}</b><br> <br> ${POEM.poemdata}
							</div>
						</c:if>
					</div>
					<br>
				</div>
			</div>
	</section>
			<c:if test="${EXISTS == 'true'}">
				<jsp:include page="songs_list.jsp" />
			</c:if>
			
	</div>
</div>

