<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="row-fluid">
	<div class="span10 offset1">
		<h2 class="page-title">
			음악평점매기기 <small>rating</small>
		</h2>
	</div>
</div>
<div class="row-fluid">
	<div class="span10 offset1">
		<section class="widget">
			<header>
				<h4>
					<i class="icon-edit text-align-center"></i>음악평가하기
					<div class="pull-right">
						<button class="btn btn-small btn-success"
							onclick="window.location.reload(true);return false;">
							다른곡 평가 하기 <i class="icon-refresh"></i>
						</button>
					</div>

				</h4>
				<hr />
				<img src="img/soul.jpg" style="height: 180px; width: 100%">
				<div></div>
			</header>

			<jsp:include page="songs_list.jsp" />
	</div>
</div>

