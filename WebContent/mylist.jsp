<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="row-fluid">
	<div class="span10 offset1">
		<h2 class="page-title">
			음악 서랍장 <small>내 리스트</small>
		</h2>
	</div>
</div>

<div class="row-fluid">
	<div class="span10 offset1">
		<section class="widget">
			<header>
				<h5>
					<i class="icon-folder-open"></i> 내 리스트&nbsp&nbsp&nbsp
				</h5>
			</header>
			<div class="actions" style="margin-top: 10px; margin-right: 13px;">
				<button class="btn btn-primary" data-toggle="modal"
					data-target="#addModal" onclick="$('#listname').val('')">추가</button>

				<div id="addModal" class="modal fade" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true"
					style="display: none;">
					<form name="frm_add" action="MylistAdd.do" method="post">
						<div class="modal-dialog">
							<div class="modal-content">

								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"
										aria-hidden="true">×</button>
									<h4 class="modal-title" id="myModalLabel">리스트 추가</h4>
								</div>
								<div class="modal-body">
									<h4 for="listname">리스트 이름을 입력하세요</h4>

									<div class="col-sm-12">
										<input type="text" id="listname" name="listname"
											style="width: 95%">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">취소</button>
									<button type="button" class="btn btn-primary"
										onclick="addList()">추가</button>
								</div>

							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</form>
				</div>

				<button class="btn btn-primary" onclick="removeList()">삭제</button>
				<div id="removeModal" class="modal fade" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true"
					style="display: none;">
					<form name="frm_remove" action="MylistRemove.do" method="post">
						<input id="remove-idx" name="remove-idx" type="hidden" value="">
					</form>
				</div>

				<button class="btn btn-primary" data-toggle="modal"
					data-target="#editModal" onclick="setEditList()">이름 편집</button>
				<div id="editModal" class="modal fade" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true"
					style="display: none;">
					<form name="frm_edit" action="MylistEdit.do" method="post">
						<input id="edit-idx" name="edit-idx" type="hidden" value="">
						<div class="modal-dialog">
							<div class="modal-content">

								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"
										aria-hidden="true">×</button>
									<h4 class="modal-title" id="myModalLabel">리스트 이름 편집</h4>
								</div>
								<div class="modal-body">
									<h4 for="editname">리스트 이름을 입력하세요</h4>

									<div class="col-sm-12">
										<input type="text" id="editname" name="editname"
											style="width: 95%">
									</div>


								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">취소</button>
									<button type="button" class="btn btn-primary"
										onclick="editList()">저장</button>
								</div>

							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</form>
				</div>
			</div>

			<div class="body">
				<div class="tabbable tabs-left hidden-phone">
					<ul id="listTab" class="nav nav-tabs">
						<c:set var="isactive" value="active" />
						<c:forEach var="mylist" items="${PLAYLIST}">
							<li id="cate-${mylist.listidx}" class="${isactive}"><a
								href="#list${mylist.listidx}" data-toggle="tab">${mylist.title}
									(${mylist.songcount})</a> <input type="hidden"
								value="${mylist.title}"></li>
							<c:set var="isactive" value="" />
						</c:forEach>
					</ul>

					<div id="listTabContent" class="tab-content">

						<c:set var="isactive" value="in active" />
						<c:forEach var="mylist" items="${PLAYLIST}">
							<div class="tab-pane fade ${isactive}" id="list${mylist.listidx}">
								<p>
									<i class="icon-list"></i><c:out value=" 플레이리스트  <${mylist.title}> 의 목록" />
									&nbsp;&nbsp;
									<button class="btn btn-success"
										onclick="playSelect('chk-c-${mylist.listidx}')">선택재생</button>
									<button class="btn btn-warning"
										onclick="removeSelect('chk-c-${mylist.listidx}')">노래제거</button>
								</p>


								<table id="folder-view" class="folder-view table table-striped">
									<thead>
										<tr>
											<th colspan="2" id="folder-actions"><input
												type="checkbox"
												onclick="checkAll('chk-c-${mylist.listidx}', this.checked)" />
											</th>
											<th>아티스트</th>
											<th colspan="2">곡명</th>
											<th colspan="1" class="total-pages">장르</th>
										</tr>
									</thead>
									<tbody>

										<c:forEach var="song" items="${mylist.songs}">
											<tr>
												<td class="tiny-column"><input type="checkbox"
													id="chk-p-${song.iidx}" class="chk-c-${mylist.listidx}" />
													<div id="div-p-${song.iidx}" style="display: none">
														<input type="hidden" class="midx" value="${song.midx}">
														<input type="hidden" class="artist" value="${song.artist}">
														<input type="hidden" class="title" value="${song.title}">
													</div></td>
												<td class="tiny-column"><i class="fa fa-star"></i></td>
												<td class="name hidden-xs"><a href="detailSearch.do?query=${song.artist}&amp;type=ARTISTMUSIC&amp;page=1">${song.artist}</a></td>
												<td class="subject">
												<a href="#" onclick="return playYoutube({midx:'${song.midx}',artist:'${fn:replace(song.artist,"'","\\'")}',title:'${fn:replace(song.title,"'","\\'")}'});">${song.title}</a></td>
												<td class="tiny-column"><i class="fa fa-paper-clip"></i></td>
												<td class="date">${song.genre}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>


							</div>
							<c:set var="isactive" value="" />
						</c:forEach>
					</div>
				</div>
				<div class="body tab-content visible-phone">
					<ul id="listTab" class="nav nav-tabs">
						<c:set var="isactive" value="active" />
						<c:forEach var="mylist" items="${PLAYLIST}">
							<li id="cate-${mylist.listidx}" class="${isactive}"><a
								href="#list${mylist.listidx}" data-toggle="tab">${mylist.title}
									(${mylist.songcount})</a> <input type="hidden"
								value="${mylist.title}"></li>
							<c:set var="isactive" value="" />
						</c:forEach>
					</ul>

					<div id="listTabContent" class="tab-content">

						<c:set var="isactive" value="in active" />
						<c:forEach var="mylist" items="${PLAYLIST}">
							<div class="tab-pane fade ${isactive}" id="list${mylist.listidx}">
								<p>
									<i class="icon-list"></i><c:out value=" 플레이리스트  <${mylist.title}> 의 목록" />
									&nbsp;&nbsp;
									<button class="btn btn-success"
										onclick="playSelect('chk-c-${mylist.listidx}')">선택재생</button>
									<button class="btn btn-warning"
										onclick="removeSelect('chk-c-${mylist.listidx}')">노래제거</button>
								</p>


								<table id="folder-view" class="folder-view table table-striped">
									<thead>
										<tr>
											<th colspan="2" id="folder-actions"><input
												type="checkbox"
												onclick="checkAll('chk-c-${mylist.listidx}', this.checked)" />
											</th>
											<th>아티스트</th>
											<th colspan="2">곡명</th>
											<th colspan="1" class="total-pages">장르</th>
										</tr>
									</thead>
									<tbody>

										<c:forEach var="song" items="${mylist.songs}">
											<tr>
												<td class="tiny-column"><input type="checkbox"
													id="chk-p-${song.iidx}" class="chk-c-${mylist.listidx}" />
													<div id="div-p-${song.iidx}" style="display: none">
														<input type="hidden" class="midx" value="${song.midx}">
														<input type="hidden" class="artist" value="${song.artist}">
														<input type="hidden" class="title" value="${song.title}">
													</div></td>
												<td class="tiny-column"><i class="fa fa-star"></i></td>
												<td class="name hidden-xs">${song.artist}</td>
												<td class="subject">${song.title}</td>
												<td class="tiny-column"><i class="fa fa-paper-clip"></i></td>
												<td class="date">${song.genre}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>


							</div>
							<c:set var="isactive" value="" />
						</c:forEach>

					</div>
				</div>
				<form name="frm_songremove" action="ItemRemove.do" method="post">
					<input id="songremove-idx" name="songremove-idx" type="hidden"
						value="">
				</form>
			</div>
		</section>
	</div>

</div>
