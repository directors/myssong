<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
<title>My Ear's SSONG</title>
<link href="css/rating_songs.css" rel="stylesheet" />
<link href="css/application.min.css" rel="stylesheet" />
<link rel="stylesheet" href="css/jquery.raty.css">
<link rel="shortcut icon" href="img/favicon.png" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="" />
<meta charset="utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<body class="background">
	<div class="logo">
		<h4>
			내귀에 <strong>SSONG</strong>
		</h4>
	</div>

	<div class="wrap">
		<div class="content container-fluid">
			<div class="row-fluid">
				<div class="span10 offset1">
					<section class="widget">
						<header>
							<h3>
								<i class="icon-edit text-align-center"></i>음악평가하기
							</h3>
							<hr>
						</header>
						<div class="body">
							<div>
								<div class="pull-right">
									<button class="btn btn-primary " onclick="setdb()">
										다른곡 <i class="icon-refresh"></i>
									</button>

									<button class="btn btn-success finishbtn" style="display: none"
										onclick="setdb(1)">
										Finish <i class="icon-ok"></i>
									</button>
								</div>
								<h4 class="text-align-center">최소 30개에 대하여 평가하여 주세요.</h4>

								<div class="progress">
									<div id="progressbar" class="progress-bar text-align-right"
										style="height: 22px; display: block;">${RATINGCOUNT}개</div>
								</div>
							</div>
							<c:forEach var="musics" items="${MUSICLIST}" varStatus="counter">
								<c:if test="${counter.count % 4 == 1}">
									<ul class="thumbnails ">
								</c:if>
								<li class="span3 ">
									<div class="thumbnail">
										<img class="albumjacket" src="img/${musics.imagePath}" />
										<div class="bottom">
											<h4>${musics.artist}-${musics.title}</h4>
										</div>
										<div class="rating">
											<div class="ratingType" id="${musics.midx}"
												style="cursor: pointer; margin-top: 10px;"></div>
										</div>
									</div>
								</li>
								<c:if test="${counter.count % 4 == 0}">
									</ul>
								</c:if>
							</c:forEach>
						</div>
						<ul class="pager wizard">
							<li class="next" style="">
								<button id="setrating" class="btn btn-primary pull-right"
									onclick="setdb()" style="margin-left:20px">
									다른곡 <i class="icon-refresh"></i>
								</button>
								<button class="btn btn-danger pull-right" onclick="setdb()">
									<a href="logout.do" class="link">
										<i class="icon-signout"></i> 로그아웃
								</a>
								</button>
							</li>
							<li class="finishbtn" style="display: none">
								<button class="btn btn-success pull-right" onclick="setdb(1)">
									Finish <i class="icon-ok"></i>
								</button>
							</li>
						</ul>
					</section>
				</div>
			</div>
		</div>
	</div>

	<!-- jquery and friends -->
	<!--    <script src="lib/jquery/jquery.1.9.0.min.js"> -->
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	<script type="text/javascript"
		src="http://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="js/jquery.raty.js"></script>
	<script type="text/javascript" src="js/ratingsongs/rating_songs.js"></script>
	<script src="lib/jquery/jquery-migrate-1.1.0.min.js"></script>

	<!-- jquery plugins -->
	<script src="lib/jquery-maskedinput/jquery.maskedinput.js"></script>
	<script src="lib/parsley/parsley.js">
      
   </script>
	<script src="lib/uniform/js/jquery.uniform.js"></script>
	<script src="lib/select2.js"></script>


	<!--backbone and friends -->
	<script src="lib/backbone/underscore-min.js"></script>

	<!-- bootstrap default plugins -->
	<script src="js/bootstrap/bootstrap-transition.js"></script>
	<script src="js/bootstrap/bootstrap-collapse.js"></script>
	<script src="js/bootstrap/bootstrap-alert.js"></script>
	<script src="js/bootstrap/bootstrap-tooltip.js"></script>
	<script src="js/bootstrap/bootstrap-popover.js"></script>
	<script src="js/bootstrap/bootstrap-button.js"></script>
	<script src="js/bootstrap/bootstrap-dropdown.js"></script>
	<script src="js/bootstrap/bootstrap-modal.js"></script>
	<script src="js/bootstrap/bootstrap-tab.js"></script>

	<!-- bootstrap custom plugins -->
	<script src="lib/bootstrap-datepicker.js"></script>
	<script src="lib/bootstrap-select/bootstrap-select.js"></script>
	<script src="lib/wysihtml5/wysihtml5-0.3.0_rc2.js"></script>
	<script src="lib/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

	<!-- basic application js-->
	<script src="js/app.js"></script>
	<script src="js/settings.js"></script>
	<script type="text/javascript">
   $(function() {
      $( "#progressbar" ).progressbar({
        value: ${RATINGCOUNT}
       }).data("value", ${RATINGCOUNT});
      });
   if(${RATINGCOUNT} < 30){
      $("#progressbar").css("width", ((${RATINGCOUNT}) / 30 * 100) + "%");
   }else{
      $("#progressbar").css("width", 100 + "%");
      $('.finishbtn').css("display","block");
   }
   $(document).ready(function() { $("#progressbar").text(${RATINGCOUNT} + "개");});
   </script>
</body>
</html>