<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	String gourl = (String)request.getAttribute("gourl");
%>

<script>
	setTimeout("document.location.href = '<%=gourl%>';", 1);
</script>