<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="row-fluid">
	<div class="span10 offset1">
		<section class="widget padding-increased">
			<header>
				<h4>
					<i class="icon-list"></i> '${QUERY }' Search Results
				</h4>
				<c:if test="${TYPE eq 'MUSIC' || TYPE eq 'ARTISTMUSIC' }">
					<div class="actions">
						<button class="btn btn-small btn-inverse" onclick="checkAll2(this, 'change-password')">
							<i class="icon-ok"></i> 전체 선택
						</button>
						<button class="btn btn-small btn-warning" onclick="playYoutube();">
							<i class="icon-play-circle"></i> 재생
						</button>
						<button type="button" class="btn btn-small btn-primary"
							data-toggle="modal" data-target="#myModal2">
							<i class="eicon-plus-circled"></i> 음악 서랍장에 추가
						</button>

					</div>
				</c:if>
				<hr />
			</header>
			<div class="body no-margin">
				<div class="body row-fluid">
					<c:forEach var="musics" items="${RESULTMUSICLIST}"
						varStatus="counter">
						<c:if test="${counter.count % 4 == 1}">
							<ul class="thumbnails ">
						</c:if>
						<li class="span3 ">
							<div class="thumbnail">
								<img class="albumjacket" src="img/${musics.imagePath}" alt="" />
								<div class="pull-left predicted-rating">
									<c:if test="${musics.rating!=0.0}">
										<strong>${musics.rating}</strong>
									</c:if>
								</div>
								<c:if test="${TYPE =='MUSIC' ||  TYPE=='ARTISTMUSIC' }">
									<label class="checkbox pull-right"> <input
										type="checkbox" id="change-password" name="change-password" class="change-password"
										artist="${musics.artist}" title="${musics.title}"
										midx="${musics.midx}">
									</label>
								</c:if>
								<div class="bottom">
									<h4>
										<c:choose>
											<c:when test="${TYPE eq 'MUSIC' || TYPE eq 'ARTISTMUSIC'}">
												<a href="#"
													onclick="return playYoutube({midx:'${musics.midx}',artist:'${fn:replace(musics.artist,"'","\\'")}',title:'${fn:replace(musics.title,"'","\\'")}'});">${musics.artist}-${musics.title}</a>
											</c:when>
											<c:when test="${TYPE eq 'ARTIST' }">

												<a href="detailSearch.do?query=${fn:replace(musics.artist,"
													&","%26")}&type=ARTISTMUSIC&page=1">${musics.artist}</a>
											</c:when>
										</c:choose>
									</h4>
								</div>
								<c:if test="${TYPE eq 'MUSIC' || TYPE eq 'ARTISTMUSIC'}">
									<div class="rating">
						<div class="ratingType" style="cursor: pointer;"
							id="${musics.midx}"></div>
					</div>
								</c:if>
							</div>
						</li>
						<c:if test="${counter.count % 4 == 0}">
							</ul>
						</c:if>
					</c:forEach>
				</div>
				<div class="pagination text-align-center">
					<ul>
						<c:choose>
							<c:when test="${BEFORE}">
								<li><a
									href="detailSearch.do?query=${fn:replace(QUERY
									,"&","%26")}&type=${TYPE}&page=${PAGEBEGIN-1}">Prev</a>
								</li>
							</c:when>
							<c:otherwise>
								<li class="disabled"><a href="#" onclick="return false;">Prev</a>
								</li>
							</c:otherwise>
						</c:choose>
						<c:forEach var="a" begin="${PAGEBEGIN}" end="${PAGEEND}" step="1">
							<c:if test="${PAGEOFFSET == a}">
								<li class="active"><a onclick="return false;"
									href="detailSearch.do?query=${fn:replace(QUERY,"
									&","%26")}&type=${TYPE}&page=${a}">${a}</a>
							</c:if>
							<c:if test="${PAGEOFFSET != a}">
								<li><a href="detailSearch.do?query=${fn:replace(QUERY,"
									&","%26")}&type=${TYPE}&page=${a}">${a}</a>
							</c:if>
							</li>
						</c:forEach>
						<c:if test="${NEXT}">
							<li><a href="detailSearch.do?query=${fn:replace(QUERY,"
								&","%26")}&type=${TYPE}&page=${PAGEEND+1}">Next</a></li>
						</c:if>
					</ul>
				</div>
			</div>
		</section>
	</div>
</div>