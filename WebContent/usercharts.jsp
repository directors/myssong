<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="row-fluid">
	<div class="span12">
		<h2 class="page-title">
			취향분석 <small>chart&graph</small>
		</h2>
	</div>
</div>
<div class="row-fluid">
	<div class="span10 offset1">
		<section class="widget">
			<header>
				<h4>
					<i class="icon-list-ol"> 추천음악</i>
				</h4>
				<hr />
			</header>

			<jsp:include page="stat_charts.jsp" />
	</div>
</div>

