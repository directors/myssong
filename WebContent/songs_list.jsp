<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<section class="widget">
	<header>
		<h4>
			<i class="eicon-note"></i>곡
		</h4>
		<div class="actions">
			<button class="btn btn-small btn-inverse" onclick="checkAll2(this, 'change-password')">
				<i class="icon-ok"></i> 전체 선택
			</button>
			
			<button class="btn btn-small btn-warning" onclick="playYoutube();">
				<i class="icon-play-circle"></i> 재생
			</button>

			<button type="button" class="btn btn-small btn-primary"
				data-toggle="modal" data-target="#myModal2">
				<i class="eicon-plus-circled"></i> 음악 서랍장에 추가
			</button>
		</div>
		<hr>
	</header>
	<div class="body row-fluid">
		<c:forEach var="musics" items="${RESULTMUSICLIST}" varStatus="counter">
			<c:if test="${counter.count % 4 == 1}">
				<ul class="thumbnails ">
			</c:if>
			<li class="span3 ">
				<div class="thumbnail">
					<img class="albumjacket" src="img/${musics.imagePath }" alt="" />
					<div class="pull-left predicted-rating">
						<c:if test="${musics.rating!=0.0}">
							<strong style="">${musics.rating}</strong>
						</c:if>
					</div>
					<label class="checkbox pull-right"> <input type="checkbox"
						id="change-password" name="change-password" class="change-password"
						artist="${musics.artist}" title="${musics.title}"
						midx="${musics.midx}">
					</label>
					<div class="bottom">
						<h4>
							<a href="#" class="hidden-phone"
								onclick="return playYoutube({midx:'${musics.midx}',artist:'${fn:replace(musics.artist,"'","\\'")}',title:'${fn:replace(musics.title,"'","\\'")}'});">${musics.artist}-${musics.title}</a>
							<a href="#" class="visible-phone">${musics.artist}-${musics.title}</a>
						</h4>
					</div>

					<div class="rating">
						<div class="ratingType" style="cursor: pointer; margin-top: 10px;"
							id="${musics.midx}"></div>
					</div>
				</div>


			</li>
			<c:if test="${counter.count % 4 == 0}">
				</ul>
			</c:if>
		</c:forEach>
	</div>
	
		<header>
		<c:if test="${EXISTS == 'true'}">
				<h4>
					<i class="eicon-note"></i> 
				</h4>
				<div class="actions">
					<button class="btn btn-small btn-success" onclick="window.location.reload(true);return false;">
									더 추천 받기 <i class="icon-refresh"></i>
								</button>
				</div>
				<hr>
		</c:if>
		</header>
</section>

