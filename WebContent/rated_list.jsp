<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="row-fluid">
	<div class="span10 offset1">
		<h2 class="page-title">
			내가 평가한 음악 <small>recommand</small>
		</h2>
	</div>
</div>
<div class="row-fluid">
	<div class="span10 offset1">
		<section class="widget">
			<header>
				<h4>
					<i class="icon-list-alt"> 평가한 음악</i>
				</h4>
				<hr />
				<img src="img/soul.jpg" style="height: 180px;width:100%">
			</header>
			<jsp:include page="ratedsongs_list.jsp" />
	</div>
</div>

